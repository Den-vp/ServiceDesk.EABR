﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Identity.Models
{
    public class LoginViewModel1
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }
}
