﻿using IdentityServer4.Models;

namespace ServiceDesk.Identity.Models
{
    public class ErrorViewModel1
    {

        public ErrorViewModel1()
        {
        }

        public ErrorViewModel1(string error)
        {
            Error = new ErrorMessage { Error = error };
        }

        public ErrorMessage Error { get; set; }
    }
}
