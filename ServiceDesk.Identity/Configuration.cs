﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityModel;

namespace ServiceDesk.Identity
{
    public static class Configuration
    {
        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("ServiceDeskWebAPI", "Web API"),
                new ApiScope("ServiceDeskAPI", "API")
            };
        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>
            {
                new ApiResource("ServiceDeskWebAPI", "Web API", new []
                    { JwtClaimTypes.Name, "role" })
                {
                    Scopes = {"ServiceDeskWebAPI"}
                }
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "servicedesk",
                    ClientName = "servicedesk Client",
                    // secret for authentication
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    // scopes that client has access to
                    AllowedScopes = { "ServiceDeskAPI" }
                },
                new Client
                {
                    ClientId = "sevicedesk-web-app",
                    ClientName = "ServiceDesk Web",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = { "https://localhost:44335/signin-oidc" },
                    PostLogoutRedirectUris = { "https://localhost:44335/signout-callback-oidc" },
                    //AllowedCorsOrigins = { "https://localhost:44335" },
                    //FrontChannelLogoutUri = "https://localhost:44335/signout-oidc",
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "ServiceDeskWebAPI"
                    }
                }
            };

    }
}
