2021-11-09 21:37:34.198 +06:00 [DBG] Adding idp claim with value: local
2021-11-09 21:37:34.200 +06:00 [DBG] Adding auth_time claim with value: 1636472254
2021-11-09 21:37:34.200 +06:00 [INF] AuthenticationScheme: Identity.Application signed in.
2021-11-09 21:37:34.203 +06:00 [INF] {"Username":"test","Provider":null,"ProviderUserId":null,"SubjectId":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","DisplayName":"test","Endpoint":"UI","ClientId":"sevicedesk-web-app","Category":"Authentication","Name":"User Login Success","EventType":"Success","Id":1000,"Message":null,"ActivityId":"80000093-0005-fd00-b63f-84710c7967bb","TimeStamp":"2021-11-09T15:37:34.0000000Z","ProcessId":35696,"LocalIpAddress":"::1:44350","RemoteIpAddress":"::1","$type":"UserLoginSuccessEvent"}
2021-11-09 21:37:34.208 +06:00 [DBG] Request path /connect/authorize/callback matched to endpoint type Authorize
2021-11-09 21:37:34.208 +06:00 [DBG] Endpoint enabled: Authorize, successfully created handler: IdentityServer4.Endpoints.AuthorizeCallbackEndpoint
2021-11-09 21:37:34.208 +06:00 [INF] Invoking IdentityServer endpoint: IdentityServer4.Endpoints.AuthorizeCallbackEndpoint for /connect/authorize/callback
2021-11-09 21:37:34.208 +06:00 [DBG] Start authorize callback request
2021-11-09 21:37:34.208 +06:00 [DBG] User in authorize request: 9a2d6121-3043-4cc9-a2c3-07730835ca6d
2021-11-09 21:37:34.208 +06:00 [DBG] Start authorize request protocol validation
2021-11-09 21:37:34.208 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.208 +06:00 [DBG] Checking for PKCE parameters
2021-11-09 21:37:34.208 +06:00 [DBG] Calling into custom validator: IdentityServer4.Validation.DefaultCustomAuthorizeRequestValidator
2021-11-09 21:37:34.208 +06:00 [DBG] ValidatedAuthorizeRequest
{"ClientId":"sevicedesk-web-app","ClientName":"ServiceDesk Web","RedirectUri":"https://localhost:44335/signin-oidc","AllowedRedirectUris":["https://localhost:44335/signin-oidc"],"SubjectId":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","ResponseType":"code","ResponseMode":"form_post","GrantType":"authorization_code","RequestedScopes":"openid profile","State":"CfDJ8N0-fN3dC75NnzItj246gN1n_Nla8KvdPL-BQ-efXZsKcgPTjlRd8ahtEEvP_6IpN3hDO1uJ-qD8GPk9FA4FWjKqa1MbwSwB6lweFRNYJBM-XYsVzC5VXXjxTtNItW5SrJ89EaTyW7E6hyQ-BTqBdwY3ZPg_g0UGKrOzcN_p02I9ulJDDgG1OHlSL2dGUJm1yeysS5q0kgUJ8lyEfKhH890ySvoCEePPz_JEeBsTcsmXV0Y2q5GPag4j_6vf0FI4MB4r2Fhht0QbydWyTQESFAFb8eCC1FLtVy4CEuUoe__NnAASPFkkZbLuM_XfbfY_CjdzsFrwNQ9FD5yOkuIVtBNj9f5i03oa9M1PKnBL4hIErPKILQIIPo0Ts3RndwT9Tw","UiLocales":null,"Nonce":"637720690455155116.MDNlMzQxMTgtZTZlZC00ZDA1LWE4ODQtZWQxMTdlNThjNzg2MWM5MjUzYWUtYTY4My00YjQ0LTkyNmMtZTZjOWU0MDhhNDBi","AuthenticationContextReferenceClasses":null,"DisplayMode":null,"PromptMode":"","MaxAge":null,"LoginHint":null,"SessionId":"6AB007815437A6CA5317CD47ACC2A02A","Raw":{"client_id":"sevicedesk-web-app","redirect_uri":"https://localhost:44335/signin-oidc","response_type":"code","scope":"openid profile","code_challenge":"aNgwVQQoXWWUGmA4CWcJAEia53ZUI2UXVCGY4NY96uk","code_challenge_method":"S256","response_mode":"form_post","nonce":"637720690455155116.MDNlMzQxMTgtZTZlZC00ZDA1LWE4ODQtZWQxMTdlNThjNzg2MWM5MjUzYWUtYTY4My00YjQ0LTkyNmMtZTZjOWU0MDhhNDBi","state":"CfDJ8N0-fN3dC75NnzItj246gN1n_Nla8KvdPL-BQ-efXZsKcgPTjlRd8ahtEEvP_6IpN3hDO1uJ-qD8GPk9FA4FWjKqa1MbwSwB6lweFRNYJBM-XYsVzC5VXXjxTtNItW5SrJ89EaTyW7E6hyQ-BTqBdwY3ZPg_g0UGKrOzcN_p02I9ulJDDgG1OHlSL2dGUJm1yeysS5q0kgUJ8lyEfKhH890ySvoCEePPz_JEeBsTcsmXV0Y2q5GPag4j_6vf0FI4MB4r2Fhht0QbydWyTQESFAFb8eCC1FLtVy4CEuUoe__NnAASPFkkZbLuM_XfbfY_CjdzsFrwNQ9FD5yOkuIVtBNj9f5i03oa9M1PKnBL4hIErPKILQIIPo0Ts3RndwT9Tw","x-client-SKU":"ID_NETSTANDARD2_0","x-client-ver":"6.7.1.0"},"$type":"AuthorizeRequestValidationLog"}
2021-11-09 21:37:34.213 +06:00 [DBG] Client is configured to not require consent, no consent is required
2021-11-09 21:37:34.213 +06:00 [DBG] Creating Authorization Code Flow response.
2021-11-09 21:37:34.224 +06:00 [INF] {"ClientId":"sevicedesk-web-app","ClientName":"ServiceDesk Web","RedirectUri":"https://localhost:44335/signin-oidc","Endpoint":"Authorize","SubjectId":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","Scopes":"openid profile","GrantType":"authorization_code","Tokens":[{"TokenType":"code","TokenValue":"****F03B","$type":"Token"}],"Category":"Token","Name":"Token Issued Success","EventType":"Success","Id":2000,"Message":null,"ActivityId":"80000175-0003-fc00-b63f-84710c7967bb","TimeStamp":"2021-11-09T15:37:34.0000000Z","ProcessId":35696,"LocalIpAddress":"::1:44350","RemoteIpAddress":"::1","$type":"TokenIssuedSuccessEvent"}
2021-11-09 21:37:34.226 +06:00 [DBG] Authorize endpoint response
{"SubjectId":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","ClientId":"sevicedesk-web-app","RedirectUri":"https://localhost:44335/signin-oidc","State":"CfDJ8N0-fN3dC75NnzItj246gN1n_Nla8KvdPL-BQ-efXZsKcgPTjlRd8ahtEEvP_6IpN3hDO1uJ-qD8GPk9FA4FWjKqa1MbwSwB6lweFRNYJBM-XYsVzC5VXXjxTtNItW5SrJ89EaTyW7E6hyQ-BTqBdwY3ZPg_g0UGKrOzcN_p02I9ulJDDgG1OHlSL2dGUJm1yeysS5q0kgUJ8lyEfKhH890ySvoCEePPz_JEeBsTcsmXV0Y2q5GPag4j_6vf0FI4MB4r2Fhht0QbydWyTQESFAFb8eCC1FLtVy4CEuUoe__NnAASPFkkZbLuM_XfbfY_CjdzsFrwNQ9FD5yOkuIVtBNj9f5i03oa9M1PKnBL4hIErPKILQIIPo0Ts3RndwT9Tw","Scope":"openid profile","Error":null,"ErrorDescription":null,"$type":"AuthorizeResponseLog"}
2021-11-09 21:37:34.226 +06:00 [DBG] Augmenting SignInContext
2021-11-09 21:37:34.226 +06:00 [INF] AuthenticationScheme: Identity.Application signed in.
2021-11-09 21:37:34.290 +06:00 [DBG] Request path /connect/token matched to endpoint type Token
2021-11-09 21:37:34.291 +06:00 [DBG] Endpoint enabled: Token, successfully created handler: IdentityServer4.Endpoints.TokenEndpoint
2021-11-09 21:37:34.291 +06:00 [INF] Invoking IdentityServer endpoint: IdentityServer4.Endpoints.TokenEndpoint for /connect/token
2021-11-09 21:37:34.291 +06:00 [DBG] Start token request.
2021-11-09 21:37:34.291 +06:00 [DBG] Start client validation
2021-11-09 21:37:34.291 +06:00 [DBG] Start parsing Basic Authentication secret
2021-11-09 21:37:34.291 +06:00 [DBG] Start parsing for secret in post body
2021-11-09 21:37:34.291 +06:00 [DBG] Parser found secret: PostBodySecretParser
2021-11-09 21:37:34.291 +06:00 [DBG] Secret id found: sevicedesk-web-app
2021-11-09 21:37:34.291 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.291 +06:00 [DBG] Secret validator success: HashedSharedSecretValidator
2021-11-09 21:37:34.291 +06:00 [DBG] Client validation success
2021-11-09 21:37:34.291 +06:00 [INF] {"ClientId":"sevicedesk-web-app","AuthenticationMethod":"SharedSecret","Category":"Authentication","Name":"Client Authentication Success","EventType":"Success","Id":1010,"Message":null,"ActivityId":"8000006d-0005-f700-b63f-84710c7967bb","TimeStamp":"2021-11-09T15:37:34.0000000Z","ProcessId":35696,"LocalIpAddress":"::1:44350","RemoteIpAddress":"::1","$type":"ClientAuthenticationSuccessEvent"}
2021-11-09 21:37:34.291 +06:00 [DBG] Start token request validation
2021-11-09 21:37:34.291 +06:00 [DBG] Start validation of authorization code token request
2021-11-09 21:37:34.291 +06:00 [DBG] Client required a proof key for code exchange. Starting PKCE validation
2021-11-09 21:37:34.297 +06:00 [DBG] Validation of authorization code token request success
2021-11-09 21:37:34.297 +06:00 [INF] Token request validation success, {"ClientId":"sevicedesk-web-app","ClientName":"ServiceDesk Web","GrantType":"authorization_code","Scopes":null,"AuthorizationCode":"****F03B","RefreshToken":"********","UserName":null,"AuthenticationContextReferenceClasses":null,"Tenant":null,"IdP":null,"Raw":{"client_id":"sevicedesk-web-app","client_secret":"***REDACTED***","code":"1E8A6EC0F81BECAE824F9B557E7D5EEE54C6C66B3D69150E3C30DD98568BF03B","grant_type":"authorization_code","redirect_uri":"https://localhost:44335/signin-oidc","code_verifier":"cMSmXJhX8JSCUW1OeUA1Aadsuc1q6WTXy_sJxI4Wouc"},"$type":"TokenRequestValidationLog"}
2021-11-09 21:37:34.297 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.297 +06:00 [DBG] Getting claims for access token for client: sevicedesk-web-app
2021-11-09 21:37:34.297 +06:00 [DBG] Getting claims for access token for subject: 9a2d6121-3043-4cc9-a2c3-07730835ca6d
2021-11-09 21:37:34.323 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.323 +06:00 [DBG] Getting claims for identity token for subject: 9a2d6121-3043-4cc9-a2c3-07730835ca6d and client: sevicedesk-web-app
2021-11-09 21:37:34.323 +06:00 [DBG] In addition to an id_token, an access_token was requested. No claims other than sub are included in the id_token. To obtain more user claims, either use the user info endpoint or set AlwaysIncludeUserClaimsInIdToken on the client configuration.
2021-11-09 21:37:34.324 +06:00 [INF] {"ClientId":"sevicedesk-web-app","ClientName":"ServiceDesk Web","RedirectUri":null,"Endpoint":"Token","SubjectId":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","Scopes":"openid profile","GrantType":"authorization_code","Tokens":[{"TokenType":"id_token","TokenValue":"****K_aw","$type":"Token"},{"TokenType":"access_token","TokenValue":"****jI7A","$type":"Token"}],"Category":"Token","Name":"Token Issued Success","EventType":"Success","Id":2000,"Message":null,"ActivityId":"8000006d-0005-f700-b63f-84710c7967bb","TimeStamp":"2021-11-09T15:37:34.0000000Z","ProcessId":35696,"LocalIpAddress":"::1:44350","RemoteIpAddress":"::1","$type":"TokenIssuedSuccessEvent"}
2021-11-09 21:37:34.324 +06:00 [DBG] Token request success.
2021-11-09 21:37:34.326 +06:00 [DBG] Request path /connect/userinfo matched to endpoint type Userinfo
2021-11-09 21:37:34.326 +06:00 [DBG] Endpoint enabled: Userinfo, successfully created handler: IdentityServer4.Endpoints.UserInfoEndpoint
2021-11-09 21:37:34.326 +06:00 [INF] Invoking IdentityServer endpoint: IdentityServer4.Endpoints.UserInfoEndpoint for /connect/userinfo
2021-11-09 21:37:34.326 +06:00 [DBG] Start userinfo request
2021-11-09 21:37:34.326 +06:00 [DBG] Bearer token found in header
2021-11-09 21:37:34.326 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.327 +06:00 [DBG] client configuration validation for client sevicedesk-web-app succeeded.
2021-11-09 21:37:34.330 +06:00 [DBG] Calling into custom token validator: IdentityServer4.Validation.DefaultCustomTokenValidator
2021-11-09 21:37:34.330 +06:00 [DBG] Token validation success
{"ClientId":null,"ClientName":null,"ValidateLifetime":true,"AccessTokenType":"Jwt","ExpectedScope":"openid","TokenHandle":null,"JwtId":"C755AD6B91E95DFE11A7E5B597588CD1","Claims":{"nbf":1636472254,"exp":1636475854,"iss":"https://localhost:44350","aud":"https://localhost:44350/resources","client_id":"sevicedesk-web-app","sub":"9a2d6121-3043-4cc9-a2c3-07730835ca6d","auth_time":1636472254,"idp":"local","role":"user","jti":"C755AD6B91E95DFE11A7E5B597588CD1","sid":"6AB007815437A6CA5317CD47ACC2A02A","iat":1636472254,"scope":["openid","profile"],"amr":"pwd"},"$type":"TokenValidationLog"}
2021-11-09 21:37:34.330 +06:00 [DBG] Creating userinfo response
2021-11-09 21:37:34.330 +06:00 [DBG] Scopes in access token: openid profile
2021-11-09 21:37:34.330 +06:00 [DBG] Requested claim types: sub name family_name given_name middle_name nickname preferred_username profile picture website gender birthdate zoneinfo locale updated_at
2021-11-09 21:37:34.350 +06:00 [INF] Profile service returned the following claim types: sub name given_name family_name website name preferred_username role
2021-11-09 21:37:34.350 +06:00 [DBG] End userinfo request
