﻿using IdentityModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using ServiceDesk.Identity.Data;
using ServiceDesk.Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Identity
{
    public static class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<AuthDbContext>(options =>
               options.UseSqlite(connectionString));

            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<AuthDbContext>();
                    DbInitializer.Initialize(context);
                    context.Database.Migrate();

                    var roleMgr = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                    var user = roleMgr.FindByNameAsync("user").Result;
                    if (user == null)
                    {
                        user = new IdentityRole
                        {
                            Name = "user"
                        };
                        _ = roleMgr.CreateAsync(user).Result;
                    }

                    var moderator = roleMgr.FindByNameAsync("moderator").Result;
                    if (moderator == null)
                    {
                        moderator = new IdentityRole
                        {
                            Name = "moderator"
                        };
                        _ = roleMgr.CreateAsync(moderator).Result;
                    }

                    var executor = roleMgr.FindByNameAsync("executor").Result;
                    if (executor == null)
                    {
                        executor = new IdentityRole
                        {
                            Name = "executor"
                        };
                        _ = roleMgr.CreateAsync(executor).Result;
                    }

                    var admin = roleMgr.FindByNameAsync("admin").Result;
                    if (admin == null)
                    {
                        admin = new IdentityRole
                        {
                            Name = "admin"
                        };
                        _ = roleMgr.CreateAsync(admin).Result;
                    }

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                    var userAdmin = userMgr.FindByNameAsync("AdminServiceDesk").Result;
                    if (userAdmin == null)
                    {
                        userAdmin = new AppUser
                        {
                            UserName = "AdminServiceDesk",
                            FirstName = "Admin",
                            LastName = "ServiceDesk",
                            Email = "eabrServiceDesk@eabr.org",
                            EmailConfirmed = true,
                        };
                        var result = userMgr.CreateAsync(userAdmin, "Admin123!").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(userAdmin, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "AdminServiceDesk"),
                            new Claim(JwtClaimTypes.GivenName, "AdminServiceDesk"),
                            new Claim(JwtClaimTypes.FamilyName, "Eabr"),
                            new Claim(JwtClaimTypes.WebSite, "https://eabrservicedesk.org"),
                            new Claim("location", "eabr"),
                            new Claim("name", "AdminServiceDesk"),
                            new Claim("role", "admin"),

                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        if (!userMgr.IsInRoleAsync(userAdmin, admin.Name).Result)
                        {
                            _ = userMgr.AddToRoleAsync(userAdmin, admin.Name).Result;
                        }

                        Log.Debug("AdminServiceDesk created");
                    }
                    else
                    {
                        Log.Debug("AdminServiceDesk already exists");
                    }

                    var userTest = userMgr.FindByNameAsync("TestServiceDesk").Result;
                    if (userTest == null)
                    {
                        userTest = new AppUser
                        {
                            UserName = "TestServiceDesk",
                            FirstName = "Test",
                            LastName = "ServiceDesk",
                            Email = "eabrTestServiceDesk@eabr.org",
                            EmailConfirmed = true
                        };
                        var result = userMgr.CreateAsync(userTest, "Start123!").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(userTest, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "TestServiceDesk"),
                            new Claim(JwtClaimTypes.GivenName, "TestServiceDesk"),
                            new Claim(JwtClaimTypes.FamilyName, "Eabr"),
                            new Claim(JwtClaimTypes.WebSite, "https://eabrservicedesk.org"),
                            new Claim("name", "TestServiceDesk"),
                            new Claim("role", "user"),

                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        if (!userMgr.IsInRoleAsync(userTest, user.Name).Result)
                        {
                            _ = userMgr.AddToRoleAsync(userTest, user.Name).Result;
                        }

                        Log.Debug("TestServiceDesk created");
                    }
                    else
                    {
                        Log.Debug("TestServiceDesk already exists");
                    }
                }
            }
        }
    }
}
