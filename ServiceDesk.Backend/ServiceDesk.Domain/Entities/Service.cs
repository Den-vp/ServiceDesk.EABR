﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Domain.Entities
{
    //этот класс нам в бэке не нужен, в фронте будут вестись услуги сюда будет прокидываться ID услуги
    public class Service : EntityBase
    {
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
