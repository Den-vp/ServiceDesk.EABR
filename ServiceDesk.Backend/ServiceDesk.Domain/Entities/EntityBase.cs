﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Domain.Entities
{
    public abstract class EntityBase
    {
        [Required]
        public Guid Id { get; set; }
    }
}
