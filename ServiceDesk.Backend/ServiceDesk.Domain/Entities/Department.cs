﻿using System;
using System.Collections.Generic;

namespace ServiceDesk.Domain.Entities
{
    public class Department : EntityBase
    {
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }

    }
}
