﻿using Microsoft.EntityFrameworkCore;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.Interfaces
{
    public interface IServiceDeskDbContext
    {
        public DbSet<Request> Requests { get; set; }
        public DbSet<Lifecycle> Lifecycles { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Activ> Activs { get; set; }
        public DbSet<Service> Services { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
