﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Application.Common.Exceptions
{
    public class NotDeleteException : Exception
    {
        public NotDeleteException(string name, object key)
            : base($"Entity \"{name}\"({key}) Заявка в обработке.") { }
    }

}
