﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestDetails
{
    public class GetRequestDetailsQueryValidator : AbstractValidator<GetRequestDetailsQuery>
    {
        public GetRequestDetailsQueryValidator()
        {
            RuleFor(req => req.Id).NotEqual(Guid.Empty);
        }
    }
}
