﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestDetails
{
    public class RequestDetailsVm : IMapWith<Request>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid? ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public Guid LifecycleId { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Request, RequestDetailsVm>()
                .ForMember(reqVm => reqVm.Id,
                    opt => opt.MapFrom(req => req.Id))
                .ForMember(reqVm => reqVm.Name,
                    opt => opt.MapFrom(req => req.Name))
                .ForMember(reqVm => reqVm.Description,
                    opt => opt.MapFrom(req => req.Description))
                .ForMember(reqVm => reqVm.Comment,
                    opt => opt.MapFrom(req => req.Comment))
                .ForMember(reqVm => reqVm.Status,
                    opt => opt.MapFrom(req => req.Status))
                .ForMember(reqVm => reqVm.Priority,
                    opt => opt.MapFrom(req => req.Priority))
                .ForMember(reqVm => reqVm.ActivId,
                    opt => opt.MapFrom(req => req.ActivId))
                .ForMember(reqVm => reqVm.File,
                    opt => opt.MapFrom(req => req.File))
                .ForMember(reqVm => reqVm.ServiceId,
                    opt => opt.MapFrom(req => req.ServiceId))
                .ForMember(reqVm => reqVm.UserId,
                    opt => opt.MapFrom(req => req.UserId))
                .ForMember(reqVm => reqVm.ExecutorId,
                    opt => opt.MapFrom(req => req.ExecutorId))
                .ForMember(reqVm => reqVm.ExecutorName,
                    opt => opt.MapFrom(req => req.ExecutorName))
                .ForMember(reqVm => reqVm.ExecutorLogin,
                    opt => opt.MapFrom(req => req.ExecutorLogin))
                .ForMember(reqVm => reqVm.ExecutorEmail,
                    opt => opt.MapFrom(req => req.ExecutorEmail))
                .ForMember(reqVm => reqVm.LifecycleId,
                    opt => opt.MapFrom(req => req.LifecycleId))
                .ForMember(lfVm => lfVm.Opened,
                    opt => opt.MapFrom(req => req.Lifecycle.Opened))
                .ForMember(lfVm => lfVm.Distributed,
                    opt => opt.MapFrom(req => req.Lifecycle.Distributed))
                .ForMember(lfVm => lfVm.Proccesing,
                    opt => opt.MapFrom(req => req.Lifecycle.Proccesing))
                .ForMember(lfVm => lfVm.Checking,
                    opt => opt.MapFrom(req => req.Lifecycle.Checking))
                .ForMember(lfVm => lfVm.Closed,
                    opt => opt.MapFrom(req => req.Lifecycle.Closed));
        }
    }
}
