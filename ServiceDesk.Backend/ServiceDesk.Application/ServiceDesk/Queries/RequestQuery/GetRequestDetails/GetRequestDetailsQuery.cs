﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestDetails
{
    public class GetRequestDetailsQuery : IRequest<RequestDetailsVm>
    {
        public Guid Id { get; set; }
    }
}
