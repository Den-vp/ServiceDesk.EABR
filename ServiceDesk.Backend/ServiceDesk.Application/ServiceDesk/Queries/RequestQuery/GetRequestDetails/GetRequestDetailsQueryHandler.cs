﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestDetails
{
    public class GetRequestDetailsQueryHandler : IRequestHandler<GetRequestDetailsQuery, RequestDetailsVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetRequestDetailsQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<RequestDetailsVm> Handle(GetRequestDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Requests
                .Include(req => req.Lifecycle)
                .FirstOrDefaultAsync(req =>
                req.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Request), request.Id);
            }

            return _mapper.Map<RequestDetailsVm>(entity);
        }
    }
}
