﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList
{
    public class RequestLookupDto : IMapWith<Request>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid? ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public Guid LifecycleId { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Request, RequestLookupDto>()
                .ForMember(reqDto => reqDto.Id,
                    opt => opt.MapFrom(req => req.Id))
                .ForMember(reqDto => reqDto.Name,
                    opt => opt.MapFrom(req => req.Name))
                .ForMember(reqDto => reqDto.Description,
                    opt => opt.MapFrom(req => req.Description))
                .ForMember(reqDto => reqDto.Comment,
                    opt => opt.MapFrom(req => req.Comment))
                .ForMember(reqDto => reqDto.Status,
                    opt => opt.MapFrom(req => req.Status))
                .ForMember(reqDto => reqDto.Priority,
                    opt => opt.MapFrom(req => req.Priority))
                .ForMember(reqDto => reqDto.ActivId,
                    opt => opt.MapFrom(req => req.ActivId))
                .ForMember(reqDto => reqDto.File,
                    opt => opt.MapFrom(req => req.File))
                .ForMember(reqDto => reqDto.ServiceId,
                    opt => opt.MapFrom(req => req.ServiceId))
                .ForMember(reqDto => reqDto.UserId,
                    opt => opt.MapFrom(req => req.UserId))
                .ForMember(reqDto => reqDto.ExecutorId,
                    opt => opt.MapFrom(req => req.ExecutorId))
                .ForMember(reqDto => reqDto.ExecutorLogin,
                    opt => opt.MapFrom(req => req.ExecutorLogin))
                .ForMember(reqDto => reqDto.ExecutorName,
                    opt => opt.MapFrom(req => req.ExecutorName))
                .ForMember(reqDto => reqDto.ExecutorEmail,
                    opt => opt.MapFrom(req => req.ExecutorEmail))
                .ForMember(reqDto => reqDto.LifecycleId,
                    opt => opt.MapFrom(req => req.LifecycleId))
                .ForMember(lfDto => lfDto.Opened,
                    opt => opt.MapFrom(req => req.Lifecycle.Opened))
                .ForMember(lfDto => lfDto.Distributed,
                    opt => opt.MapFrom(req => req.Lifecycle.Distributed))
                .ForMember(lfDto => lfDto.Proccesing,
                    opt => opt.MapFrom(req => req.Lifecycle.Proccesing))
                .ForMember(lfDto => lfDto.Checking,
                    opt => opt.MapFrom(req => req.Lifecycle.Checking))
                .ForMember(lfDto => lfDto.Closed,
                    opt => opt.MapFrom(req => req.Lifecycle.Closed));
        }
    }
}
