﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList
{
    public class GetRequestListQuery : IRequest<RequestListVm>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid UserId { get; set; }
        public string UserCreated { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }
        public Guid LifecycleId { get; set; }
        public bool ExecuterFl { get; set; }

    }
}
