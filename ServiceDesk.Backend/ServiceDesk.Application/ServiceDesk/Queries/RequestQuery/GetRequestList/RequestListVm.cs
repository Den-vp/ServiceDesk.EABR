﻿using System.Collections.Generic;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList
{
    public class RequestListVm
    {
        public IList<RequestLookupDto> Requests { get; set; }
    }
}
