﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList
{
    public class GetRequestListQueryHandler : IRequestHandler<GetRequestListQuery, RequestListVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IServiceDeskDbContextUser _dbContextUser;
        private readonly IMapper _mapper;

        public GetRequestListQueryHandler(IServiceDeskDbContext dbContext, IServiceDeskDbContextUser dbContextUser,
            IMapper mapper) =>
            (_dbContext, _dbContextUser, _mapper) = (dbContext, dbContextUser, mapper);

        public async Task<RequestListVm> Handle(GetRequestListQuery request,
            CancellationToken cancellationToken)
        {
            List<RequestLookupDto> reqQuery;
            var user = string.IsNullOrEmpty(request.UserCreated) ? "" : _dbContextUser.Users.Where(usr => usr.UserName == request.UserCreated).Select(usr => usr.Id).FirstOrDefault();
            if (request.ExecuterFl)
            {
                reqQuery = await _dbContext.Requests
                    .Where(req => (req.ExecutorId == Guid.Empty || !req.ExecutorId.HasValue) && (string.IsNullOrEmpty(user) || req.UserId == new Guid(user)))
                    .ProjectTo<RequestLookupDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
            }
            else
            {
                reqQuery = await _dbContext.Requests
                    .Where(req => string.IsNullOrEmpty(user) || req.UserId == new Guid(user))
                    .ProjectTo<RequestLookupDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
            }
            return new RequestListVm { Requests = reqQuery };
        }
    }
}
