﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList
{
    public class GetRequestListQueryValidator : AbstractValidator<GetRequestListQuery>
    {
        public GetRequestListQueryValidator()
        {
            RuleFor(x => x.Name).NotEqual(string.Empty);
            RuleFor(x => x.Status).NotNull();
            RuleFor(x => x.Priority).NotNull();
            RuleFor(x => x.UserId).NotNull();
            RuleFor(x => x.LifecycleId).NotNull();
        }
    }
}
