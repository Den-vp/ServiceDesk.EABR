﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList
{
    public class GetActivListQueryValidator : AbstractValidator<GetActivListQuery>
    {
        public GetActivListQueryValidator()
        {
            RuleFor(x => x.CabNumber).NotEqual(string.Empty);
        }
    }
}
