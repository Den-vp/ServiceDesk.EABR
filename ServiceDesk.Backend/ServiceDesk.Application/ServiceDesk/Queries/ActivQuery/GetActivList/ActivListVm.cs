﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList
{
    public class ActivListVm
    {
        public IList<ActivLookupDto> Activs { get; set; }
    }
}
