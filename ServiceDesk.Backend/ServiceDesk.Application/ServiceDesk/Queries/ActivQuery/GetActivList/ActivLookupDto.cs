﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList
{
    public class ActivLookupDto : IMapWith<Activ>
    {
        public Guid Id { get; set; }
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<Activ, ActivLookupDto>()
                .ForMember(actDto => actDto.Id,
                    opt => opt.MapFrom(act => act.Id))
                .ForMember(actDto => actDto.CabNumber,
                    opt => opt.MapFrom(act => act.CabNumber))
                .ForMember(actDto => actDto.DepartmentId,
                    opt => opt.MapFrom(act => act.DepartmentId));
        }
    }
}
