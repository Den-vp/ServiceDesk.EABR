﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivList
{
    public class GetActivListQueryHandler : IRequestHandler<GetActivListQuery, ActivListVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetActivListQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<ActivListVm> Handle(GetActivListQuery request,
            CancellationToken cancellationToken)
        {
            var actQuery = await _dbContext.Activs
                .Where(act => String.IsNullOrEmpty(request.DepartmentName) || act.Department.Name.StartsWith(request.DepartmentName))
                .ProjectTo<ActivLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
            return new ActivListVm { Activs = actQuery };
        }
    }
}
