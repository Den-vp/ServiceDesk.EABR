﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivDetails
{
    public class GetActivDetailsQueryValidator : AbstractValidator<GetActivDetailsQuery>
    {
        public GetActivDetailsQueryValidator()
        {
            RuleFor(act => act.Id).NotEqual(Guid.Empty);
        }
    }
}
