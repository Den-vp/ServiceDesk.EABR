﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivDetails
{
    public class ActivDetailsVm : IMapWith<Activ>
    {
        public Guid Id { get; set; }
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<Activ, ActivDetailsVm>()
                .ForMember(actVm => actVm.Id,
                    opt => opt.MapFrom(act => act.Id))
                .ForMember(actVm => actVm.CabNumber,
                    opt => opt.MapFrom(act => act.CabNumber))
                .ForMember(actVm => actVm.DepartmentId,
                    opt => opt.MapFrom(act => act.DepartmentId))
                .ForMember(actVm => actVm.CreationDate,
                    opt => opt.MapFrom(act => act.CreationDate))
                .ForMember(actVm => actVm.EditDate,
                    opt => opt.MapFrom(act => act.EditDate));
        }
    }
}
