﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.ActivQuery.GetActivDetails
{
    public class GetActivDetailsQueryHandler
     : IRequestHandler<GetActivDetailsQuery, ActivDetailsVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetActivDetailsQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<ActivDetailsVm> Handle(GetActivDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Activs
                .FirstOrDefaultAsync(dep =>
                dep.Id == request.Id, cancellationToken);

            if (entity == null /*|| entity.UserId != request.UserId*/)
            {
                throw new NotFoundException(nameof(Activ), request.Id);
            }

            return _mapper.Map<ActivDetailsVm>(entity);
        }
    }
}
