﻿using FluentValidation;
using System;


namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserDetails
{
    public class GetUserDetailsQueryValidator : AbstractValidator<GetUserDetailsQuery>
    {
        public GetUserDetailsQueryValidator()
        {
            RuleFor(usr => usr.Id).NotEqual(string.Empty);
        }
    }
}
