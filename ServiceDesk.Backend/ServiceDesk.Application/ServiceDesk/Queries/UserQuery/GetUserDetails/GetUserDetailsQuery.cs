﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserDetails
{
    public class GetUserDetailsQuery : IRequest<UserDetailsVm>
    {
        public string Id { get; set; }
    }
}
