﻿using MediatR;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserList
{
    public class GetUserListQuery : IRequest<UserListVm>
    {
        public string Name { get; set; }
    }
}
