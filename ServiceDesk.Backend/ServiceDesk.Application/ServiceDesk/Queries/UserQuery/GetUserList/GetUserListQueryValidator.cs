﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserList
{
    public class GetUserListQueryValidator : AbstractValidator<GetUserListQuery>
    {
        public GetUserListQueryValidator()
        {
            RuleFor(x => x.Name).NotEqual(string.Empty);
        }
    }
}
