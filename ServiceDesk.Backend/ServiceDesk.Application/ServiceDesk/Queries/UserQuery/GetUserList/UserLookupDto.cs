﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.UserQuery.GetUserList
{
    public class UserLookupDto : IMapWith<AppUser>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<AppUser, UserLookupDto>()
                .ForMember(usrDto => usrDto.Id,
                    opt => opt.MapFrom(usr => usr.Id))
                .ForMember(usrDto => usrDto.Name,
                    opt => opt.MapFrom(usr => $"{usr.FirstName} {usr.LastName}"))
                .ForMember(usrDto => usrDto.Login,
                    opt => opt.MapFrom(usr => usr.UserName))
                .ForMember(usrDto => usrDto.Email,
                    opt => opt.MapFrom(usr => usr.Email))
                ;
        }
    }
}
