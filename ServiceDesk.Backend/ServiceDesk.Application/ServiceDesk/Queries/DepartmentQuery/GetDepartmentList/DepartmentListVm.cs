﻿using System.Collections.Generic;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList
{
    public class DepartmentListVm
    {
        public IList<DepartmentLookupDto> Departments { get; set; }
    }
}
