﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList
{
    public class DepartmentLookupDto : IMapWith<Department>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Department, DepartmentLookupDto>()
                .ForMember(depDto => depDto.Id,
                    opt => opt.MapFrom(dep => dep.Id))
                .ForMember(depDto => depDto.Name,
                    opt => opt.MapFrom(dep => dep.Name));
        }
    }
}
