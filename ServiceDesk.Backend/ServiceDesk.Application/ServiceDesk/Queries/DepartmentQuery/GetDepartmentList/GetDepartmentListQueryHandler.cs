﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList
{
    public class GetDepartmentListQueryHandler : IRequestHandler<GetDepartmentListQuery, DepartmentListVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetDepartmentListQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<DepartmentListVm> Handle(GetDepartmentListQuery request,
            CancellationToken cancellationToken)
        {
            var depQuery = await _dbContext.Departments
                .Where(dep => String.IsNullOrEmpty(request.Name) || dep.Name.StartsWith(request.Name))
                .ProjectTo<DepartmentLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return new DepartmentListVm { Departments = depQuery };
        }
    }
}
