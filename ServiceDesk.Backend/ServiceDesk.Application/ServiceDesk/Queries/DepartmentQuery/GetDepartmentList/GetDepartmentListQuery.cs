﻿using MediatR;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList
{
    public class GetDepartmentListQuery : IRequest<DepartmentListVm>
    {
        public string Name { get; set; }
    }
}
