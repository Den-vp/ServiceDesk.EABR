﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList
{
    public class GetDepartmentListQueryValidator : AbstractValidator<GetDepartmentListQuery>
    {
        public GetDepartmentListQueryValidator()
        {
            RuleFor(x => x.Name).NotEqual(string.Empty);
        }
    }
}
