﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails
{
    public class GetDepartmentDetailsQueryHandler 
        : IRequestHandler<GetDepartmentDetailsQuery, DepartmentDetailsVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetDepartmentDetailsQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) => (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<DepartmentDetailsVm> Handle(GetDepartmentDetailsQuery request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Departments
                .FirstOrDefaultAsync(dep =>
                dep.Id == request.Id, cancellationToken);

            if (entity == null /*|| entity.UserId != request.UserId*/)
            {
                throw new NotFoundException(nameof(Department), request.Id);
            }

            return _mapper.Map<DepartmentDetailsVm>(entity);
        }
    }
}
