﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails
{
    public class GetDepartmentDetailsQuery : IRequest<DepartmentDetailsVm>
    {
        public Guid Id { get; set; }
    }
}
