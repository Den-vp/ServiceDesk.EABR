﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails
{
    public class DepartmentDetailsVm : IMapWith<Department>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<Department, DepartmentDetailsVm>()
                .ForMember(depVm => depVm.Name,
                    opt => opt.MapFrom(dep => dep.Name))
                .ForMember(depVm => depVm.Id,
                    opt => opt.MapFrom(dep => dep.Id))
                .ForMember(depVm => depVm.CreationDate,
                    opt => opt.MapFrom(dep => dep.CreationDate))
                .ForMember(depVm => depVm.EditDate,
                    opt => opt.MapFrom(dep => dep.EditDate));
        }
    }
}
