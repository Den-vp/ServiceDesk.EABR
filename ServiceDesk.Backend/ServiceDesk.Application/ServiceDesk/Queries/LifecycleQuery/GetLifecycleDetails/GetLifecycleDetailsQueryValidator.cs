﻿using FluentValidation;
using System;


namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleDetails
{
    public class GetLifecycleDetailsQueryValidator : AbstractValidator<GetLifecycleDetailsQuery>
    {
        public GetLifecycleDetailsQueryValidator()
        {
            RuleFor(lf => lf.Id).NotEqual(Guid.Empty);
        }
    }
}
