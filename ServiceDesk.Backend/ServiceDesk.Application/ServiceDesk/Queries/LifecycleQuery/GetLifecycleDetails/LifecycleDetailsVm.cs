﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleDetails
{
    public class LifecycleDetailsVm : IMapWith<Lifecycle>
    {
        public Guid Id { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }



        public void Mapping(Profile profile)
        {
            profile.CreateMap<Lifecycle, LifecycleDetailsVm>()
                .ForMember(lcVm => lcVm.Id,
                    opt => opt.MapFrom(lc => lc.Id))
                .ForMember(lcVm => lcVm.Opened,
                    opt => opt.MapFrom(lc => lc.Opened))
                .ForMember(lcVm => lcVm.Distributed,
                    opt => opt.MapFrom(lc => lc.Distributed))
                .ForMember(lcVm => lcVm.Proccesing,
                    opt => opt.MapFrom(lc => lc.Proccesing))
                .ForMember(lcVm => lcVm.Checking,
                    opt => opt.MapFrom(lc => lc.Checking))
                .ForMember(lcVm => lcVm.Closed,
                    opt => opt.MapFrom(lc => lc.Closed));
        }
    }
}
