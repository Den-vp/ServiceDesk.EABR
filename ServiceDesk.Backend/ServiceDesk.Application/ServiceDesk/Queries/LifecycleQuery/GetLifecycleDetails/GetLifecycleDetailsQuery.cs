﻿using MediatR;
using System;


namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleDetails
{
    public class GetLifecycleDetailsQuery : IRequest<LifecycleDetailsVm>
    {
        public Guid Id { get; set; }
    }
}
