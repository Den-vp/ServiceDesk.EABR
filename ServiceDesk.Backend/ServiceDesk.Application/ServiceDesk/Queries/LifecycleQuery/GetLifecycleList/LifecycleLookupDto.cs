﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList
{
    public class LifecycleLookupDto : IMapWith<Lifecycle>
    {
        public Guid Id { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Lifecycle, LifecycleLookupDto>()
                .ForMember(lfDto => lfDto.Id,
                    opt => opt.MapFrom(lf => lf.Id))
                .ForMember(lfDto => lfDto.Opened,
                    opt => opt.MapFrom(lf => lf.Opened))
                .ForMember(lfDto => lfDto.Distributed,
                    opt => opt.MapFrom(lf => lf.Distributed))
                .ForMember(lfDto => lfDto.Proccesing,
                    opt => opt.MapFrom(lf => lf.Proccesing))
                .ForMember(lfDto => lfDto.Checking,
                    opt => opt.MapFrom(lf => lf.Checking))
                .ForMember(lfDto => lfDto.Closed,
                    opt => opt.MapFrom(lf => lf.Closed));
        }
    }
}
