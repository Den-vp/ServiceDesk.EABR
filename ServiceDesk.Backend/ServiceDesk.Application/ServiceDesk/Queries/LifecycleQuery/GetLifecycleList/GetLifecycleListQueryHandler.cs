﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList
{
    public class GetLifecycleListQueryHandler : IRequestHandler<GetLifecycleListQuery, LifecycleListVm>
    {
        private readonly IServiceDeskDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetLifecycleListQueryHandler(IServiceDeskDbContext dbContext,
            IMapper mapper) =>
            (_dbContext, _mapper) = (dbContext, mapper);

        public async Task<LifecycleListVm> Handle(GetLifecycleListQuery request,
            CancellationToken cancellationToken)
        {
            var notesQuery = await _dbContext.Departments
                .Where(lf => lf.Id.Equals(request.Id))
                .ProjectTo<LifecycleLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            return new LifecycleListVm { Lifecycles = notesQuery };
        }
    }
}
