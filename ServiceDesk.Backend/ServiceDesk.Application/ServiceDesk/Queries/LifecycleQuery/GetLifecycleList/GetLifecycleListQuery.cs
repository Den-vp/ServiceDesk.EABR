﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList
{
    public class GetLifecycleListQuery : IRequest<LifecycleListVm>
    {
        public Guid Id { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }
    }
}
