﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceDetails
{
    public class GetServiceDetailsQuery : IRequest<ServiceDetailsVm>
    {
        public Guid Id { get; set; }
    }
}
