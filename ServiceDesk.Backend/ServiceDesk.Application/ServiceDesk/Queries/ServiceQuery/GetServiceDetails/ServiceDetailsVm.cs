﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceDetails
{
    public class ServiceDetailsVm : IMapWith<Service>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }


        public void Mapping(Profile profile)
        {
            profile.CreateMap<Service, ServiceDetailsVm>()
                .ForMember(serVm => serVm.Name,
                    opt => opt.MapFrom(ser => ser.Name))
                .ForMember(serVm => serVm.Id,
                    opt => opt.MapFrom(ser => ser.Id))
                .ForMember(serVm => serVm.Subtitle,
                    opt => opt.MapFrom(ser => ser.Subtitle))
                .ForMember(serVm => serVm.CreationDate,
                    opt => opt.MapFrom(ser => ser.CreationDate))
                .ForMember(serVm => serVm.EditDate,
                    opt => opt.MapFrom(ser => ser.EditDate));
        }
    }
}
