﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceDetails
{
    public class GetServiceDetailsQueryValidator : AbstractValidator<GetServiceDetailsQuery>
    {
        public GetServiceDetailsQueryValidator()
        {
            RuleFor(ser => ser.Id).NotEqual(Guid.Empty);
        }
    }
}
