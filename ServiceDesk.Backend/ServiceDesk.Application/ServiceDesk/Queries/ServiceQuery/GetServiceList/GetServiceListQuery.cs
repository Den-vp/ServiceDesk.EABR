﻿using MediatR;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList
{
    public class GetServiceListQuery : IRequest<ServiceListVm>
    {
        public string Name { get; set; }
    }
}
