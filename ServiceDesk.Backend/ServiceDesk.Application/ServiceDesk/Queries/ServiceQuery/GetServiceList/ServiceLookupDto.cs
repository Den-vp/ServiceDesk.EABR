﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Domain.Entities;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList
{
    public class ServiceLookupDto : IMapWith<Service>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Service, ServiceLookupDto>()
                .ForMember(serDto => serDto.Id,
                    opt => opt.MapFrom(ser => ser.Id))
                .ForMember(serDto => serDto.Name,
                    opt => opt.MapFrom(ser => ser.Name))
                .ForMember(serDto => serDto.Subtitle,
                    opt => opt.MapFrom(ser => ser.Subtitle));
        }
    }
}
