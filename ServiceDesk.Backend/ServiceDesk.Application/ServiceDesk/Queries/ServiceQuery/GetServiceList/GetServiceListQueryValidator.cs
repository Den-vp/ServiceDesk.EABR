﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList
{
    public class GetServiceListQueryValidator : AbstractValidator<GetServiceListQuery>
    {
        public GetServiceListQueryValidator()
        {
            RuleFor(x => x.Name).NotEqual(string.Empty);
        }
    }
}
