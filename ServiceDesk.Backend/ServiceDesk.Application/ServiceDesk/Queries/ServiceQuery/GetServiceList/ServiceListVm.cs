﻿using System.Collections.Generic;


namespace ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList
{
    public class ServiceListVm
    {
        public IList<ServiceLookupDto> Services { get; set; }
    }
}
