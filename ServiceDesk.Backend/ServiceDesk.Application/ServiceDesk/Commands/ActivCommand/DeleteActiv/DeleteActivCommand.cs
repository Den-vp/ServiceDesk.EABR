﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.DeleteActiv
{
    public class DeleteActivCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
