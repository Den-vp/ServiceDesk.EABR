﻿using MediatR;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.DeleteActiv
{
    public class DeleteActivCommandHandler
        : IRequestHandler<DeleteActivCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public DeleteActivCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteActivCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Activs
                .FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Activ), request.Id);
            }

            _dbContext.Activs.Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
