﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.DeleteActiv
{
    public class DeleteActivCommandValidator : AbstractValidator<DeleteActivCommand>
    {
        public DeleteActivCommandValidator()
        {
            RuleFor(deleteActivCommand => deleteActivCommand.Id).NotEqual(Guid.Empty);
        }
    }
}
