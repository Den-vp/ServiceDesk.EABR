﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.UpdateActiv
{
    public class UpdateActivCommandValidator : AbstractValidator<UpdateActivCommand>
    {
        public UpdateActivCommandValidator()
        {
            RuleFor(updateActivCommand => updateActivCommand.Id).NotEqual(Guid.Empty);
            RuleFor(updateActivCommand => updateActivCommand.CabNumber).NotEmpty().MaximumLength(10).WithMessage("Превышена максимальная длинна записи");
            RuleFor(updateActivCommand => updateActivCommand.DepartmentId).NotEqual(Guid.Empty).WithMessage("Укажите Департамент");
        }
    }
}
