﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.UpdateActiv
{
    public class UpdateActivCommandHandler
        : IRequestHandler<UpdateActivCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public UpdateActivCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(UpdateActivCommand request,
            CancellationToken cancellationToken)
        {
            var entity =
                await _dbContext.Activs.FirstOrDefaultAsync(act =>
                    act.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Activ), request.Id);
            }

            entity.CabNumber = request.CabNumber;
            entity.DepartmentId = request.DepartmentId;
            entity.EditDate = DateTime.Now;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
