﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.UpdateActiv
{
    public class UpdateActivCommand : IRequest
    {
        public Guid Id { get; set; }
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }
    }
}
