﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.CreateActiv
{
    public class CreateActivCommand : IRequest<Guid>
    {
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }
    }
}
