﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.DeleteService
{
    public class DeleteServiceCommandValidator : AbstractValidator<DeleteServiceCommand>
    {
        public DeleteServiceCommandValidator()
        {
            RuleFor(deleteServiceCommand => deleteServiceCommand.Id).NotEqual(Guid.Empty);
        }
    }
}
