﻿using MediatR;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.DeleteService
{
    public class DeleteServiceCommandHandler
        : IRequestHandler<DeleteServiceCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public DeleteServiceCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteServiceCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Services
                .FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Service), request.Id);
            }

            _dbContext.Services.Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
