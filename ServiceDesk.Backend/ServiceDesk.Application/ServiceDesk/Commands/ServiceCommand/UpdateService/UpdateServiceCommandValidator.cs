﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.UpdateService
{
    public class UpdateServiceCommandValidator : AbstractValidator<UpdateServiceCommand>
    {
        public UpdateServiceCommandValidator()
        {
            RuleFor(updateServiceCommand => updateServiceCommand.Id).NotEqual(Guid.Empty);
            RuleFor(updateServiceCommand => updateServiceCommand.Name).NotEmpty().MaximumLength(50).WithMessage("Превышена максимальная длинна названия услуги");
            RuleFor(updateServiceCommand => updateServiceCommand.Name).NotEmpty().MaximumLength(100).WithMessage("Превышена максимальная длинна описания услуги");
        }
    }
}
