﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.UpdateService
{
    public class UpdateServiceCommandHandler
        : IRequestHandler<UpdateServiceCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public UpdateServiceCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(UpdateServiceCommand request,
            CancellationToken cancellationToken)
        {
            var entity =
                await _dbContext.Services.FirstOrDefaultAsync(ser =>
                    ser.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Service), request.Id);
            }

            entity.Name = request.Name;
            entity.Subtitle = request.Subtitle;
            entity.EditDate = DateTime.Now;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
