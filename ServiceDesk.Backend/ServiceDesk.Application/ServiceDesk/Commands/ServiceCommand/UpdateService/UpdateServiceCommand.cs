﻿using MediatR;
using System;


namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.UpdateService
{
    public class UpdateServiceCommand : IRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
    }
}
