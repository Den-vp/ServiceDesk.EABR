﻿using MediatR;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;


namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.CreateService
{
    public class CreateServiceCommandHandler : IRequestHandler<CreateServiceCommand, Guid>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public CreateServiceCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Guid> Handle(CreateServiceCommand request,
            CancellationToken cancellationToken)
        {
            var service = new Service
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Subtitle = request.Subtitle,
                CreationDate = DateTime.Now,
                EditDate = null
            };

            await _dbContext.Services.AddAsync(service, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return service.Id;
        }
    }
}
