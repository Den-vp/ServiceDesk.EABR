﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.CreateService
{
    public class CreateServiceCommand : IRequest<Guid>
    {
        public string Name { get; set; }
        public string Subtitle { get; set; }

    }
}
