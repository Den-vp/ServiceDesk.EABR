﻿using System;
using FluentValidation;

namespace ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.CreateService
{
    public class CreateServiceCommandValidator : AbstractValidator<CreateServiceCommand>
    {
        public CreateServiceCommandValidator()
        {
            RuleFor(createServiceCommand => createServiceCommand.Name).NotEmpty().MaximumLength(50).WithMessage("Превышена максимальная длинна названия услуги");
            RuleFor(createServiceCommand => createServiceCommand.Subtitle).NotEmpty().MaximumLength(100).WithMessage("Превышена максимальная длинна описания услуги");
        }
    }
}
