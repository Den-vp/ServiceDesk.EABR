﻿using MediatR;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.DeleteDepartment
{
    public class DeleteDepartmentCommandHandler 
        : IRequestHandler<DeleteDepartmentCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public DeleteDepartmentCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteDepartmentCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Departments
                .FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null /*|| entity.UserId != request.UserId*/)
            {
                throw new NotFoundException(nameof(Department), request.Id);
            }

            _dbContext.Departments.Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
