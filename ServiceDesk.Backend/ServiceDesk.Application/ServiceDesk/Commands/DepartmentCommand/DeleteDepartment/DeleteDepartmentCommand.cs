﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.DeleteDepartment
{
    public class DeleteDepartmentCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
