﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.DeleteDepartment
{
    public class DeleteDepartmentCommandValidator : AbstractValidator<DeleteDepartmentCommand>
    {
        public DeleteDepartmentCommandValidator()
        {
            RuleFor(deleteDepartmentCommand => deleteDepartmentCommand.Id).NotEqual(Guid.Empty);
        }
    }
}
