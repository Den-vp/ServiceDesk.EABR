﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment
{
    public class CreateDepartmentCommand : IRequest<Guid>
    {
        public string Name { get; set; }
    }
}
