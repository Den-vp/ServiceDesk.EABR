﻿using System;
using FluentValidation;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment
{
    public class CreateDepartmentCommandValidator : AbstractValidator<CreateDepartmentCommand>
    {
        public CreateDepartmentCommandValidator()
        {
            RuleFor(createDepartmentCommand =>
                createDepartmentCommand.Name).NotEmpty().MaximumLength(250);
        }
    }
}
