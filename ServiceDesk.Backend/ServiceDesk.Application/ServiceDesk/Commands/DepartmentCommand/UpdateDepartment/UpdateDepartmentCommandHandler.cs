﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment
{
    public class UpdateDepartmentCommandHandler 
        : IRequestHandler<UpdateDepartmentCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public UpdateDepartmentCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(UpdateDepartmentCommand request,
            CancellationToken cancellationToken)
        {
            var entity =
                await _dbContext.Departments.FirstOrDefaultAsync(dep =>
                    dep.Id == request.Id, cancellationToken);

            if (entity == null/* || entity.UserId != request.UserId*/)
            {
                throw new NotFoundException(nameof(Department), request.Id);
            }

            entity.Name = request.Name;
            entity.EditDate = DateTime.Now;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}