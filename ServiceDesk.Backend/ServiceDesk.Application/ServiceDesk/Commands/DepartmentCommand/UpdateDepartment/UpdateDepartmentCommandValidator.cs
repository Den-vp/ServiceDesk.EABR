﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment
{
    public class UpdateDepartmentCommandValidator : AbstractValidator<UpdateDepartmentCommand>
    {
        public UpdateDepartmentCommandValidator()
        {
            RuleFor(updateDepartmentCommand => updateDepartmentCommand.Id).NotEqual(Guid.Empty);
            RuleFor(updateDepartmentCommand => updateDepartmentCommand.Name)
                .NotEmpty().MaximumLength(250);
        }
    }
}
