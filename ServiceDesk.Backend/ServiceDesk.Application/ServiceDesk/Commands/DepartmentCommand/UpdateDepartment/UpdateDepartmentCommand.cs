﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment
{
    public class UpdateDepartmentCommand : IRequest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
