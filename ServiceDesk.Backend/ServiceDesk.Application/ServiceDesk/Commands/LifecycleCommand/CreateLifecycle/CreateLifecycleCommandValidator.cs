﻿using System;
using FluentValidation;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.CreateLifecycle
{
    public class CreateLifecycleCommandValidator : AbstractValidator<CreateLifecycleCommand>
    {
        public CreateLifecycleCommandValidator()
        {
            RuleFor(createLifecycleCommand => createLifecycleCommand.Opened).NotNull().WithMessage("Дата создания не может быть пустой");
        }
    }
}
