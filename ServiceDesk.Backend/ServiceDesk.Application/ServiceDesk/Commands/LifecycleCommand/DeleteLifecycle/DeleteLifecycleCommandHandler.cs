﻿using MediatR;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.DeleteLifecycle
{
    public class DeleteLifecycleCommandHandler
        : IRequestHandler<DeleteLifecycleCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public DeleteLifecycleCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteLifecycleCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Lifecycles
                .FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Lifecycle), request.Id);
            }
            if (entity.Distributed != null)
            {
                throw new NotDeleteException(nameof(Lifecycle), request.Id);
            }

            _dbContext.Lifecycles.Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
