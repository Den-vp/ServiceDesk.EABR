﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.DeleteLifecycle
{
    public class DeleteLifecycleCommandValidator : AbstractValidator<DeleteLifecycleCommand>
    {
        public DeleteLifecycleCommandValidator()
        {
            RuleFor(deleteLifecycleCommand => deleteLifecycleCommand.Id).NotEqual(Guid.Empty);
        }
    }
}
