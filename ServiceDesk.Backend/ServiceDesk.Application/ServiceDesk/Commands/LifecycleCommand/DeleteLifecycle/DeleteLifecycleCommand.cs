﻿using MediatR;
using System;


namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.DeleteLifecycle
{
    public class DeleteLifecycleCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
