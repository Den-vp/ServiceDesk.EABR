﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.UpdateLifecycle
{
    public class UpdateLifecycleCommand : IRequest
    {
        public Guid Id { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

    }
}
