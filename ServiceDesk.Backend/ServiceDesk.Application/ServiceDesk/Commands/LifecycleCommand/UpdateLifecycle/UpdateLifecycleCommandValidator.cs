﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.UpdateLifecycle
{
    public class UpdateLifecycleCommandValidator : AbstractValidator<UpdateLifecycleCommand>
    {
        public UpdateLifecycleCommandValidator()
        {
            RuleFor(updateLifecycleCommand => updateLifecycleCommand.Id).NotEqual(Guid.Empty);
            RuleFor(updateLifecycleCommand => updateLifecycleCommand.Proccesing).NotNull().WithMessage("Дата обработки не может быть пустой");
        }
    }
}
