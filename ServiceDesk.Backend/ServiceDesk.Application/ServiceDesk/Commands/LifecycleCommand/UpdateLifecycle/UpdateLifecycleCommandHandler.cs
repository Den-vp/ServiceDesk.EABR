﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.UpdateLifecycle
{
    public class UpdateLifecycleCommandHandler
        : IRequestHandler<UpdateLifecycleCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public UpdateLifecycleCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(UpdateLifecycleCommand request,
            CancellationToken cancellationToken)
        {
            var entity =
                await _dbContext.Lifecycles.FirstOrDefaultAsync(act =>
                    act.Id == request.Id, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Lifecycle), request.Id);
            }

            entity.Distributed = request.Distributed;
            entity.Proccesing = request.Proccesing;
            entity.Checking = request.Checking;
            entity.Closed = request.Closed;

            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
