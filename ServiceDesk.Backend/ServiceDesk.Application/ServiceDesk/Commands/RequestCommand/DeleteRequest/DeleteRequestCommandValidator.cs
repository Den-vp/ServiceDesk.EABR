﻿using FluentValidation;
using System;


namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.DeleteRequest
{
    public class DeleteRequestCommandValidator : AbstractValidator<DeleteRequestCommand>
    {
        public DeleteRequestCommandValidator()
        {
            RuleFor(deleteRequestCommand => deleteRequestCommand.Id).NotEqual(Guid.Empty);
        }
    }
}
