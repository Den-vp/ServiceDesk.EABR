﻿using MediatR;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.DeleteRequest
{
    public class DeleteRequestCommandHandler
        : IRequestHandler<DeleteRequestCommand>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public DeleteRequestCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Unit> Handle(DeleteRequestCommand request,
            CancellationToken cancellationToken)
        {
            var entity = await _dbContext.Requests
                .FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Request), request.Id);
            }

            _dbContext.Requests.Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
