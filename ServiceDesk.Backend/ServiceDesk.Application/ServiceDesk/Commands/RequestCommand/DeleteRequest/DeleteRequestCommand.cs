﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.DeleteRequest
{
    public class DeleteRequestCommand : IRequest
    {
        public Guid Id { get; set; }
    }
}
