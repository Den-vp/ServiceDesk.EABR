﻿using MediatR;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.CreateRequest
{
    public class CreateRequestCommandHandler : IRequestHandler<CreateRequestCommand, Guid>
    {
        private readonly IServiceDeskDbContext _dbContext;

        public CreateRequestCommandHandler(IServiceDeskDbContext dbContext) =>
            _dbContext = dbContext;

        public async Task<Guid> Handle(CreateRequestCommand request,
            CancellationToken cancellationToken)
        {
            var LifecycleId = Guid.NewGuid();
            var _requestLifecycle = new Lifecycle
            {
                Id = LifecycleId,
                Opened = request.Opened,
                Distributed = request.Distributed,
                Proccesing = request.Proccesing,
                Checking = request.Checking,
                Closed = null
            };
            await _dbContext.Lifecycles.AddAsync(_requestLifecycle, cancellationToken);
            var _request = new Request
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Description = request.Description,
                Comment = request.Comment,
                Status = request.Status,
                Priority = request.Priority,
                ActivId = request.ActivId,
                File = request.File,
                ServiceId = request.ServiceId,
                UserId = request.UserId,
                ExecutorId = request.ExecutorId,
                ExecutorLogin = request.ExecutorLogin,
                ExecutorEmail = request.ExecutorEmail,
                ExecutorName = request.ExecutorName,
                LifecycleId = LifecycleId,
                ClientEmail = request.ClientEmail
            };
            await _dbContext.Requests.AddAsync(_request, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return _request.Id;
        }
    }
}
