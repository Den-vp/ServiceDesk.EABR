﻿using System;
using FluentValidation;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.CreateRequest
{
    public class CreateRequestCommandValidator : AbstractValidator<CreateRequestCommand>
    {
        public CreateRequestCommandValidator()
        {
            RuleFor(createRequestCommand => createRequestCommand.Name).NotEmpty().MaximumLength(50).WithMessage("Превышена максимальная длинна названия заявки");
            RuleFor(createRequestCommand => createRequestCommand.Description).NotEmpty().MaximumLength(200).WithMessage("Превышена максимальная длинна описания заявки");
            RuleFor(createRequestCommand => createRequestCommand.Comment).NotEmpty().MaximumLength(500).WithMessage("Превышена максимальная длинна коментария заявки");
            RuleFor(createRequestCommand => createRequestCommand.Status).NotNull().WithMessage("Статус заявки не может быть пустым");
            RuleFor(createRequestCommand => createRequestCommand.Priority).NotNull().WithMessage("Приоритет заявки не может быть пустым");
            RuleFor(createRequestCommand => createRequestCommand.UserId).NotEqual(Guid.Empty).WithMessage("Инициатор не может быть пустым");
            RuleFor(createRequestCommand => createRequestCommand.Opened).NotEmpty().WithMessage("Дата открытия не может быть пустой");
        }
    }
}
