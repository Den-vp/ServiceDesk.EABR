﻿using MediatR;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.CreateRequest
{
    public class CreateRequestCommand : IRequest<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid? ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public Guid LifecycleId { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }
        public string ClientEmail { get; set; }
    }
}
