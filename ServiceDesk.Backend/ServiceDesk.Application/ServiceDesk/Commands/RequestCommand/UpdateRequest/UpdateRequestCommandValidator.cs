﻿using FluentValidation;
using System;

namespace ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.UpdateRequest
{
    public class UpdateRequestCommandValidator : AbstractValidator<UpdateRequestCommand>
    {
        public UpdateRequestCommandValidator()
        {
            RuleFor(updateRequestCommand => updateRequestCommand.Id).NotEqual(Guid.Empty);
            RuleFor(updateRequestCommand => updateRequestCommand.Name).NotEmpty().MaximumLength(50).WithMessage("Превышена максимальная длинна наименования");
            RuleFor(updateRequestCommand => updateRequestCommand.Description).NotEmpty().MaximumLength(200).WithMessage("Превышена максимальная длинна описания");
            RuleFor(updateRequestCommand => updateRequestCommand.Comment).NotEmpty().MaximumLength(200).WithMessage("Превышена максимальная длинна комментария");
            RuleFor(updateRequestCommand => updateRequestCommand.Status).NotNull().WithMessage("Статус не может быть пустым");
            RuleFor(updateRequestCommand => updateRequestCommand.Priority).NotNull().WithMessage("Приоритет не может быть пустым");
            RuleFor(updateRequestCommand => updateRequestCommand.ServiceId).NotEqual(Guid.Empty).WithMessage("Услуга не может быть пустой");
            RuleFor(updateRequestCommand => updateRequestCommand.UserId).NotEqual(Guid.Empty).WithMessage("Инициатор не может быть пустым");
        }
    }
}
