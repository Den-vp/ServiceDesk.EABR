﻿using RabbitMQLib;
using System;

namespace ServiceDesk.ConsumerMQ
{
    static class Program
    {
        static void Main(string[] args)
        {
            ReceiveFromMQ();
        }
        private static void ReceiveFromMQ()
        {
            var mq = new MQLib();
            Console.WriteLine("Start receive from MQ");
            mq.ConsumeDirect();
        }
    }
}
