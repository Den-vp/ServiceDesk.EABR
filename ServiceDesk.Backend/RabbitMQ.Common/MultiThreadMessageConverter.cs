﻿using OpenPop.Mime;
using ServiceDesk.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Email
{
    class MultiThreadMessageConverter
    {
        private int _threadCount;
        public MultiThreadMessageConverter(int threadcount)
        {
            _threadCount = threadcount;

        }


        public List<Request> GetRequestsParallel(List<Message> msgList)

        {
            var partSize = msgList.Count / _threadCount;
            var requestList = new List<Request>();
            var listThread = new List<Thread>();
            for (int i = 0; i < _threadCount; i++)
            {
                var query = msgList.Skip(partSize * i);
                if (i != _threadCount - 1)
                {
                    query = query.Take(partSize);
                }
                listThread.Add(new Thread(delegate () { foreach (var req in GetRequests(query.ToList())) { requestList.Add(req); } }));
                listThread[i].Start();
            }
            listThread.ForEach(x => x.Join());

            return requestList;
        }

        public List<Request> GetRequests(List<Message> msgList)
        {
            var requestList = new List<Request>();
            foreach (var msg in msgList)
            {
                requestList.Add(MsgToRequest(msg));
            }
            return requestList;
        }

        public Request MsgToRequest(Message msg)

        {
            var from = msg.Headers.From;
            var subj = msg.Headers.Subject;
            string message = "";
            var plainText = msg.FindFirstPlainTextVersion();
            if (plainText != null)
            {
                message = plainText.GetBodyAsText();
            }
            else
            {
                var html = msg.FindFirstHtmlVersion();
                if (html != null)
                {
                    message = html.GetBodyAsText();
                }

            }

            return new Request
            {
                Name = subj,
                ClientEmail = from.ToString(),
                Description = "Заявка с почты",
                Comment = message,
                Status = 0,
                Priority = 2,
                UserId = new Guid("5A2DB1C0-B99C-42BA-A141-4A2C0E544B26")
            };
        }
    }
}
