﻿using OpenPop.Pop3;

namespace ServiceDesk.Email.Concrete
{
    class EmailClient
    {
        private static Pop3Client _emailClient;

      
        public static Pop3Client GetInstance()
        {
            if (_emailClient == null)
            {
                _emailClient = new Pop3Client();
            }
            return _emailClient;
        }

        public  void Connect(string host, int port, bool useSsl)
        {
            _emailClient.Connect(host, port, useSsl);
        }

        public void Authenticate(string login, string password)
        {
            _emailClient.Authenticate(login, password);
        }

        public void Disconnect()
        {
            _emailClient.Disconnect();
        }
        
    }
}
