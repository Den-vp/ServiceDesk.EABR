﻿using ServiceDesk.Email.Abstract;
using ServiceDesk.Domain.Entities;
using Newtonsoft.Json;

namespace ServiceDesk.Email.Concrete
{
    class SerializerJson : ISerializerJson
    {
        public string SerializeToJson(Request request)
        {
            var result = JsonConvert.SerializeObject(request);
            return result;
        }
    }
}
