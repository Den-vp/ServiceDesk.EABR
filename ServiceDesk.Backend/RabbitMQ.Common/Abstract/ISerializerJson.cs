﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceDesk.Domain.Entities;
   

namespace ServiceDesk.Email.Abstract
{
    interface ISerializerJson
    {
        public string SerializeToJson(Request request);
    }
}
