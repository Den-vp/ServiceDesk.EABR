﻿using System;
using ServiceDesk.Domain.Entities;
using ServiceDesk.Email.Concrete;
using System.Collections.Generic;
using OpenPop.Mime;
using OpenPop.Pop3;
using System.Threading;
using RabbitMQLib;
namespace ServiceDesk.Email
{
    static class Program
    {
        static void Main(string[] args)
        {
            TimerCallback tm = new TimerCallback(ProcessEmail);
            _ = new Timer(tm, null, 0, 10000);
            Console.ReadKey();
        }

        static void ProcessEmail(object obj)
        {
            Console.WriteLine("Begin ProcessEmail");
            var emailClient = EmailClient.GetInstance();
            emailClient.Connect("pop.mail.ru", 995, true);
            Console.WriteLine("Connected...");
            emailClient.Authenticate("eabrsdesk@mail.ru", "MbwhM2P2ffj5hYTNLcc3");
            Console.WriteLine("Authentication passed...");
            var msgList = GetMsgList(emailClient);
            Console.WriteLine("Get messages...");
            emailClient.Disconnect();
            Console.WriteLine("Disconnected...");
            
            var messageConverter = new MultiThreadMessageConverter(2/*Environment.ProcessorCount*/);
            var requestList = messageConverter.GetRequestsParallel(msgList);
            var toJson = new SerializerJson();
            foreach (var request in requestList)
            {
                var jsonRequest = toJson.SerializeToJson(request);
                Console.WriteLine(jsonRequest);
                SendToMQ(jsonRequest);                
            }
        }
        public static List<Message> GetMsgList(Pop3Client _emailClient)
        {
            if (!_emailClient.Connected)
            {
                throw new InvalidOperationException("Нет соединения с сервером!");
            }
            var msgList = new List<Message>();
            var count = _emailClient.GetMessageCount();
            for (int i = count; i > 0; i--)
            {
                msgList.Add(_emailClient.GetMessage(i));
                _emailClient.DeleteMessage(i);
            }
            return msgList;
        }

        private static void SendToMQ(string message)
        {
           var mq = new MQLib();
            mq.ProduceDirect(message);
        }

    }
}
