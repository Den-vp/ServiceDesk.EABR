﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Domain.Entities;
using ServiceDesk.Persistence.EntityTypeConfigurations;
using System;

namespace ServiceDesk.Persistence
{
    public class ServiceDeskDbContext : DbContext, IServiceDeskDbContext
    {
        public DbSet<Request> Requests { get; set; }
        public DbSet<Lifecycle> Lifecycles { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Activ> Activs { get; set; }
        public DbSet<Service> Services { get; set; }


        public ServiceDeskDbContext(DbContextOptions<ServiceDeskDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DepartmentConfiguration());
            modelBuilder.ApplyConfiguration(new ActivConfiguration());
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Department>().HasData(new Department
            {
                Id = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                CreationDate = DateTime.Now,
                Name = "УИТ"
            });

            modelBuilder.Entity<Activ>().HasData(new Activ
            {
                Id = new Guid("d90e6d29-0d9e-4b15-bcb9-e2f3597186f0"),
                CabNumber = "4-1",
                DepartmentId = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                CreationDate = DateTime.Now,               
            });

            modelBuilder.Entity<Activ>().HasData(new Activ
            {
                Id = new Guid("b674d0ca-1aba-49fd-bf0c-fae9274d78f0"),
                CabNumber = "4-2",
                DepartmentId = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                CreationDate = DateTime.Now,
            });

            modelBuilder.Entity<Activ>().HasData(new Activ
            {
                Id = new Guid("6d1eb3a0-d3f7-437a-afcd-15d94b81ff48"),
                CabNumber = "4-3",
                DepartmentId = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                CreationDate = DateTime.Now,
            });

            modelBuilder.Entity<Department>().HasData(new Department
            {
                Id = new Guid("f2a9e75b-32d8-4238-9c7f-2cfd232f2593"),
                CreationDate = DateTime.Now,
                Name = "ОРОК"
            });

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.LogTo(Console.WriteLine);
    }
}