﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ServiceDesk.Domain.Entities;

namespace ServiceDesk.Persistence.EntityTypeConfigurations
{
    public class ActivConfiguration : IEntityTypeConfiguration<Activ>
    {
        public void Configure(EntityTypeBuilder<Activ> builder)
        {
            builder.HasKey(act => act.Id);
            builder.HasIndex(act => act.Id).IsUnique();
            builder.Property(act => act.CabNumber).HasMaxLength(50);
        }
    }
}
