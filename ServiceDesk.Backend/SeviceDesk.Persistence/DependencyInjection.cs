﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceDesk.Application.Interfaces;
using System;
using System.IO;

namespace ServiceDesk.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection
            services, IConfiguration configuration)
        {
            var connectionString = configuration["DbConnection"];
            var pathDbUser = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\..\\..\\"));
            var connectionStringUser = "Data Source = " + Path.Combine(pathDbUser, "ServiceDesk.Identity", configuration["DbConnectionUser"]);
            services
                .AddDbContext<ServiceDeskDbContext>(options =>
                {
                    options.UseSqlite(connectionString);
                })
                .AddDbContext<ServiceDeskDbContextUser>(options =>
                {
                    options.UseSqlite(connectionStringUser);
                });
            services
                .AddScoped<IServiceDeskDbContext>(provider =>
                provider.GetService<ServiceDeskDbContext>())
                .AddScoped<IServiceDeskDbContextUser>(provider =>
                provider.GetService<ServiceDeskDbContextUser>());

            return services;
        }
    }
}
