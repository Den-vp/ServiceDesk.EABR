﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQLib
{
    public class Constants
    {
        public const string Authority = "https://localhost:44350";
        public const string AuthorityMtls = "https://identityserver.local";

        public const string SampleApi = "https://localhost:44328/api/1.0/";
        public const string SampleApiMtls = "https://api.identityserver.local/";        
    }
}
