﻿using System;
using RabbitMQ.Client;
using System.Text;
using RabbitMQ.Client.Events;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using ServiceDesk.WebApi.Models.RequestModel;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Net.Http.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using IdentityModel.Client;

namespace RabbitMQLib
{
    public class MQLib
    {
        public void ProduceDirect(string message)
        {
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                var bodyDirect = Encoding.UTF8.GetBytes(message);
                //var exchangeNameDirect = "eabrsdesk-fanout";
                //var queueNameDirect = "requests-fanout";
                //var routingKey = "rk1-fanout";

                var exchangeNameDirect = "eabrsdesk-direct";
                var queueNameDirect = "requests-direct";
                var routingKey = "rk1-direct";

                channel.ExchangeDeclare(exchangeNameDirect, ExchangeType.Direct);
                channel.QueueDeclare(queueNameDirect, false, false, false, null);
                channel.QueueBind(queueNameDirect, exchangeNameDirect, routingKey, null);
                channel.BasicPublish(exchange: exchangeNameDirect,
                    routingKey: routingKey,
                    basicProperties: null,
                    body: bodyDirect
                    );
                Console.WriteLine($"Отправлено {message}");
            }
        }

        public static async Task RunAsync(CreateRequestDto requestDto)
        {
            var response = await RequestTokenAsync();
            await CallServiceAsync(response.AccessToken, requestDto);
        }

        static async Task<TokenResponse> RequestTokenAsync()
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync(Constants.Authority);
            if (disco.IsError) throw new Exception(disco.Error);

            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "servicedesk",
                ClientSecret = "secret",

                Scope = "ServiceDeskAPI"
            });

            if (response.IsError) throw new Exception(response.Error);
            return response;
        }

        static async Task CallServiceAsync(string token, CreateRequestDto requestDto)
        {
            var baseAddress = new Uri($"{Constants.SampleApi}Request");

            var client = new HttpClient();
            client.SetBearerToken(token);
            var response = await client.PostAsJsonAsync(baseAddress, requestDto);
            response.EnsureSuccessStatusCode();
            Console.WriteLine(response.ToString());
        }

        public void ConsumeDirect()
        {
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                var consumer = new EventingBasicConsumer(channel);

                //var exchangeNameDirect = "eabrsdesk-fanout";
                //var queueNameDirect = "requests-fanout";
                //var routingKey = "rk1-fanout";

                var exchangeNameDirect = "eabrsdesk-direct";
                var queueNameDirect = "requests-direct";
                var routingKey = "rk1-direct";


                channel.ExchangeDeclare(exchangeNameDirect, ExchangeType.Direct);
                channel.QueueDeclare(queueNameDirect, false, false, false, null);
                channel.QueueBind(queueNameDirect, exchangeNameDirect, routingKey, null);
                var requestDto = new CreateRequestDto();
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body.ToArray());
                    requestDto = JsonConvert.DeserializeObject<CreateRequestDto>(message);
                    requestDto.Opened = DateTime.Now;
                    requestDto.Proccesing = DateTime.Now;
                    Console.WriteLine(" [x] Received {0}", requestDto.Name);
                    //if (requestDto.) { }
                    _ = RunAsync(requestDto);
                };

                channel.BasicConsume(queue: queueNameDirect,
                                     autoAck: true,
                                     consumer: consumer);
                Console.ReadKey();
            }
        }

        static private IConnection GetRabbitConnection()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "kgnybgrk",
                Password = "U4xC-1qoO7KRjxwtqC77f_r9QBrShIyj",
                VirtualHost = "kgnybgrk",
                HostName = "kangaroo.rmq.cloudamqp.com"
            };

            IConnection conn = factory.CreateConnection();
            return conn;
        }
    }
}
