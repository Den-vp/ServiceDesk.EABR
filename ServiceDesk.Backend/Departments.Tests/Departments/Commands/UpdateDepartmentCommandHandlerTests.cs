﻿using Departments.Tests.Common;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Departments.Tests.Departments.Commands
{
    public class UpdateDepartmentCommandHandlerTests : TestCommandBase
    {
        [Fact]
        public async Task UpdateNoteCommandHandler_Success()
        {
            // Arrange
            var handler = new UpdateDepartmentCommandHandler(Context);
            var updatedName = "new name";

            // Act
            await handler.Handle(new UpdateDepartmentCommand
            {
                Id = DepartmentsContextFactory.DepartmentIdForUpdate,
                Name = updatedName
            }, CancellationToken.None);

            // Assert
            Assert.NotNull(await Context.Departments.SingleOrDefaultAsync(dep =>
                dep.Id == DepartmentsContextFactory.DepartmentIdForUpdate &&
                dep.Name == updatedName));
        }

        [Fact]
        public async Task UpdateNoteCommandHandler_FailOnWrongId()
        {
            // Arrange
            var handler = new UpdateDepartmentCommandHandler(Context);

            // Act
            // Assert
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await handler.Handle(
                    new UpdateDepartmentCommand
                    {
                        Id = Guid.NewGuid()
                    },
                    CancellationToken.None));
        }
    }
}
