﻿using Departments.Tests.Common;
using ServiceDesk.Application.Common.Exceptions;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.DeleteDepartment;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Departments.Tests.Departments.Commands
{
    public class DeleteDepartmentCommandHandlerTests : TestCommandBase
    {
        [Fact]
        public async Task DeleteNoteCommandHandler_Success()
        {
            // Arrange
            var handler = new DeleteDepartmentCommandHandler(Context);

            // Act
            await handler.Handle(new DeleteDepartmentCommand
            {
                Id = DepartmentsContextFactory.DepartmentIdForDelete
            }, CancellationToken.None);

            // Assert
            Assert.Null(Context.Departments.SingleOrDefault(dep =>
                dep.Id == DepartmentsContextFactory.DepartmentIdForDelete));
        }

        [Fact]
        public async Task DeleteNoteCommandHandler_FailOnWrongId()
        {
            // Arrange
            var handler = new DeleteDepartmentCommandHandler(Context);

            // Act
            // Assert
            await Assert.ThrowsAsync<NotFoundException>(async () =>
                await handler.Handle(
                    new DeleteDepartmentCommand
                    {
                        Id = Guid.NewGuid()
                    },
                    CancellationToken.None));
        }
    }
}
