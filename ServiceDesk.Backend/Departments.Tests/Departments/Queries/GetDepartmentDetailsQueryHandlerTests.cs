﻿using AutoMapper;
using Departments.Tests.Common;
using ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails;
using ServiceDesk.Persistence;
using Shouldly;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Departments.Tests.Departments.Queries
{
    [Collection("QueryCollection")]
    public class GetDepartmentDetailsQueryHandlerTests
    {
        private readonly ServiceDeskDbContext Context;
        private readonly IMapper Mapper;

        public GetDepartmentDetailsQueryHandlerTests(QueryTestFixture fixture)
        {
            Context = fixture.Context;
            Mapper = fixture.Mapper;
        }

        [Fact]
        public async Task GetNoteDetailsQueryHandler_Success()
        {
            // Arrange
            var handler = new GetDepartmentDetailsQueryHandler(Context, Mapper);

            // Act
            var result = await handler.Handle(
                new GetDepartmentDetailsQuery
                {
                    Id = Guid.Parse("909F7C29-891B-4BE1-8504-21F84F262084")
                },
                CancellationToken.None);

            // Assert
            result.ShouldBeOfType<DepartmentDetailsVm>();
            result.Name.ShouldBe("Test department 2");
            result.CreationDate.ShouldBe(DateTime.Today);
        }
    }
}
