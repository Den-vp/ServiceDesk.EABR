﻿using AutoMapper;
using ServiceDesk.Persistence;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList;
using Departments.Tests.Common;
using System.Threading;
using Shouldly;

namespace Departments.Tests.Departments.Queries
{
    [Collection("QueryCollection")]
    public class GetDepartmentListQueryHandlerTests
    {
        private readonly ServiceDeskDbContext Context;
        private readonly IMapper Mapper;

        public GetDepartmentListQueryHandlerTests(QueryTestFixture fixture)
        {
            Context = fixture.Context;
            Mapper = fixture.Mapper;
        }

        [Fact]
        public async Task GetNoteListQueryHandler_Success()
        {
            // Arrange
            var handler = new GetDepartmentListQueryHandler(Context, Mapper);

            // Act
            var result = await handler.Handle(
                new GetDepartmentListQuery
                {
                    Name = DepartmentsContextFactory.depName
                },
                CancellationToken.None);

            // Assert
            result.ShouldBeOfType<DepartmentListVm>();
            result.Departments.Count.ShouldBe(2);
        }
    }
}
