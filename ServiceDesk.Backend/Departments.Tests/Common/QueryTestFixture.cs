﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Persistence;
using System;
using Xunit;

namespace Departments.Tests.Common
{
    public class QueryTestFixture : IDisposable
    {
        public ServiceDeskDbContext Context;
        public IMapper Mapper;

        public QueryTestFixture()
        {
            Context = DepartmentsContextFactory.Create();
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AssemblyMappingProfile(
                    typeof(IServiceDeskDbContext).Assembly));
            });
            Mapper = configurationProvider.CreateMapper();
        }

        public void Dispose()
        {
            DepartmentsContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }

}
