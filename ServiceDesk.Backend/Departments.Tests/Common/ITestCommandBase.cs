﻿namespace Departments.Tests.Common
{
    public interface ITestCommandBase
    {
        void Dispose();
    }
}