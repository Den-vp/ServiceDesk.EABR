﻿using System;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Persistence;
using ServiceDesk.Domain.Entities;
namespace Departments.Tests.Common
{
    static class DepartmentsContextFactory
    {
        public static Guid DepartmentIdForDelete = Guid.NewGuid();
        public static Guid DepartmentIdForUpdate = Guid.NewGuid();
        public static string depName = "Test";

        public static ServiceDeskDbContext Create()
        {
            var options = new DbContextOptionsBuilder<ServiceDeskDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new ServiceDeskDbContext(options);
            context.Database.EnsureCreated();
            context.Departments.AddRange(
                new Department
                {
                    CreationDate = DateTime.Today,
                    Name = "Tes1t department 1",
                    EditDate = null,
                    Id = Guid.Parse("A6BB65BB-5AC2-4AFA-8A28-2616F675B825")
                },
                new Department
                {
                    CreationDate = DateTime.Today,
                    Name = "Test department 2",
                    EditDate = null,
                    Id = Guid.Parse("909F7C29-891B-4BE1-8504-21F84F262084")
                },
                new Department
                {
                    CreationDate = DateTime.Today,
                    Name = "Tes2t department 3",
                    EditDate = null,
                    Id = DepartmentIdForDelete
                },
                new Department
                {
                    CreationDate = DateTime.Today,
                    Name = "Test department 4",
                    EditDate = null,
                    Id = DepartmentIdForUpdate
                }
            );
            context.SaveChanges();
            return context;
        }

        public static void Destroy(ServiceDeskDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }

}