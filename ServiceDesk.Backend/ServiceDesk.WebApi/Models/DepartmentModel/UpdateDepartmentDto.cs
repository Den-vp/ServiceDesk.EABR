﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment;
using System;

namespace ServiceDesk.WebApi.Models.DepartmentModel
{
    public class UpdateDepartmentDto : IMapWith<UpdateDepartmentCommand>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateDepartmentDto, UpdateDepartmentCommand>()
                .ForMember(depCommand => depCommand.Id,
                    opt => opt.MapFrom(depDto => depDto.Id))
                .ForMember(depCommand => depCommand.Name,
                    opt => opt.MapFrom(depDto => depDto.Name));
        }
    }
}
