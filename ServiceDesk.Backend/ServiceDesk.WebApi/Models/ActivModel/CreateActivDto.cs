﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.ActivCommand.CreateActiv;
using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.WebApi.Models.ActivModel
{
    public class CreateActivDto : IMapWith<CreateActivCommand>
    {
        [Required]
        public string CabNumber { get; set; }
        public Guid DepartmentId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateActivDto, CreateActivCommand>()
                .ForMember(actCommand => actCommand.CabNumber,
                    opt => opt.MapFrom(actDto => actDto.CabNumber))
                .ForMember(actCommand => actCommand.DepartmentId,
                    opt => opt.MapFrom(actDto => actDto.DepartmentId));
        }
    }
}
