﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.UpdateRequest;
using System;

namespace ServiceDesk.WebApi.Models.RequestModel
{
    public class UpdateRequestDto : IMapWith<UpdateRequestCommand>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequestDto, UpdateRequestCommand>()
                .ForMember(reqCommand => reqCommand.Id,
                    opt => opt.MapFrom(reqDto => reqDto.Id))
                .ForMember(reqCommand => reqCommand.Name,
                    opt => opt.MapFrom(reqDto => reqDto.Name))
                .ForMember(reqCommand => reqCommand.Description,
                    opt => opt.MapFrom(reqDto => reqDto.Description))
                .ForMember(reqCommand => reqCommand.Comment,
                    opt => opt.MapFrom(reqDto => reqDto.Comment))
                .ForMember(reqCommand => reqCommand.Status,
                    opt => opt.MapFrom(reqDto => reqDto.Status))
                .ForMember(reqCommand => reqCommand.Priority,
                    opt => opt.MapFrom(reqDto => reqDto.Priority))
                .ForMember(reqCommand => reqCommand.ActivId,
                    opt => opt.MapFrom(reqDto => reqDto.ActivId))
                .ForMember(reqCommand => reqCommand.File,
                    opt => opt.MapFrom(reqDto => reqDto.File))
                .ForMember(reqCommand => reqCommand.ServiceId,
                    opt => opt.MapFrom(reqDto => reqDto.ServiceId))
                .ForMember(reqCommand => reqCommand.UserId,
                    opt => opt.MapFrom(reqDto => reqDto.UserId))
                .ForMember(reqCommand => reqCommand.ExecutorId,
                    opt => opt.MapFrom(reqDto => reqDto.ExecutorId))
                .ForMember(reqCommand => reqCommand.ExecutorName,
                    opt => opt.MapFrom(reqDto => reqDto.ExecutorName))
                .ForMember(reqCommand => reqCommand.ExecutorLogin,
                    opt => opt.MapFrom(reqDto => reqDto.ExecutorLogin))
                .ForMember(reqCommand => reqCommand.ExecutorEmail,
                    opt => opt.MapFrom(reqDto => reqDto.ExecutorEmail))
                .ForMember(reqCommand => reqCommand.Proccesing,
                    opt => opt.MapFrom(reqDto => reqDto.Proccesing))
                .ForMember(reqCommand => reqCommand.Distributed,
                    opt => opt.MapFrom(reqDto => reqDto.Distributed))
                .ForMember(reqCommand => reqCommand.Checking,
                    opt => opt.MapFrom(reqDto => reqDto.Checking));
        }
    }
}
