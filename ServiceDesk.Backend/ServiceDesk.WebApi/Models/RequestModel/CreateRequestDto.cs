﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.CreateLifecycle;
using ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.CreateRequest;
using ServiceDesk.WebApi.Models.LifecycleModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.WebApi.Models.RequestModel
{
    public class CreateRequestDto : IMapWith<CreateRequestCommand>, IMapWith<CreateLifecycleCommand>
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public Guid? ActivId { get; set; }
        public string File { get; set; }
        public Guid? ServiceId { get; set; }
        public Guid UserId { get; set; }
        public Guid? ExecutorId { get; set; }
        public string ExecutorName { get; set; }
        public string ExecutorLogin { get; set; }
        public string ExecutorEmail { get; set; }

        public Guid LifecycleId { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }
        public string ClientEmail { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateRequestDto, CreateRequestCommand>()
                .ForMember(reqCommand => reqCommand.Name,
                    opt => opt.MapFrom(reqDto => reqDto.Name))
                .ForMember(reqCommand => reqCommand.Description,
                    opt => opt.MapFrom(reqDto => reqDto.Description))
                .ForMember(reqCommand => reqCommand.Comment,
                    opt => opt.MapFrom(reqDto => reqDto.Comment))
                .ForMember(reqCommand => reqCommand.Status,
                    opt => opt.MapFrom(reqDto => reqDto.Status))
                .ForMember(reqCommand => reqCommand.Priority,
                    opt => opt.MapFrom(reqDto => reqDto.Priority))
                .ForMember(reqCommand => reqCommand.ActivId,
                    opt => opt.MapFrom(reqDto => reqDto.ActivId))
                .ForMember(reqCommand => reqCommand.File,
                    opt => opt.MapFrom(reqDto => reqDto.File))
                .ForMember(reqCommand => reqCommand.ServiceId,
                    opt => opt.MapFrom(reqDto => reqDto.ServiceId))
                .ForMember(reqCommand => reqCommand.UserId,
                    opt => opt.MapFrom(reqDto => reqDto.UserId))
                .ForMember(reqCommand => reqCommand.ExecutorId,
                    opt => opt.MapFrom(reqDto => reqDto.ExecutorId))
                .ForMember(reqCommand => reqCommand.LifecycleId,
                    opt => opt.MapFrom(reqDto => reqDto.LifecycleId));

            profile.CreateMap<CreateLifecycleDto, CreateLifecycleCommand>()
                .ForMember(lifecycleCommand => lifecycleCommand.Opened,
                    opt => opt.MapFrom(lifecycleDto => lifecycleDto.Opened))
                .ForMember(lifecycleCommand => lifecycleCommand.Distributed,
                    opt => opt.MapFrom(lifecycleDto => lifecycleDto.Distributed))
                .ForMember(lifecycleCommand => lifecycleCommand.Proccesing,
                    opt => opt.MapFrom(lifecycleDto => lifecycleDto.Proccesing))
                .ForMember(lifecycleCommand => lifecycleCommand.Checking,
                    opt => opt.MapFrom(lifecycleDto => lifecycleDto.Checking))
                .ForMember(lifecycleCommand => lifecycleCommand.Closed,
                    opt => opt.MapFrom(lifecycleDto => lifecycleDto.Closed));
        }
    }
}
