﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.CreateLifecycle;
using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.WebApi.Models.LifecycleModel
{
    public class CreateLifecycleDto : IMapWith<CreateLifecycleCommand>
    {
        [Required]
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime? Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateLifecycleDto, CreateLifecycleCommand>()
                .ForMember(lfCommand => lfCommand.Opened,
                    opt => opt.MapFrom(lfDto => lfDto.Opened))
                .ForMember(lfCommand => lfCommand.Distributed,
                    opt => opt.MapFrom(lfDto => lfDto.Distributed))
                .ForMember(lfCommand => lfCommand.Proccesing,
                    opt => opt.MapFrom(lfDto => lfDto.Proccesing))
                .ForMember(lfCommand => lfCommand.Checking,
                    opt => opt.MapFrom(lfDto => lfDto.Checking))
                .ForMember(lfCommand => lfCommand.Closed,
                    opt => opt.MapFrom(lfDto => lfDto.Closed));
        }
    }
}
