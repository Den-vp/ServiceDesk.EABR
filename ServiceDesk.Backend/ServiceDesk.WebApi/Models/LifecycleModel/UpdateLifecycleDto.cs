﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.UpdateLifecycle;
using System;

namespace ServiceDesk.WebApi.Models.LifecycleModel
{
    public class UpdateLifecycleDto : IMapWith<UpdateLifecycleCommand>
    {
        public Guid Id { get; set; }
        public DateTime Opened { get; set; }
        public DateTime? Distributed { get; set; }
        public DateTime Proccesing { get; set; }
        public DateTime? Checking { get; set; }
        public DateTime? Closed { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateLifecycleDto, UpdateLifecycleCommand>()
                .ForMember(lfCommand => lfCommand.Id,
                    opt => opt.MapFrom(lfDto => lfDto.Id))
                .ForMember(lfCommand => lfCommand.Opened,
                    opt => opt.MapFrom(lfDto => lfDto.Opened))
                .ForMember(lfCommand => lfCommand.Distributed,
                    opt => opt.MapFrom(lfDto => lfDto.Distributed))
                .ForMember(lfCommand => lfCommand.Proccesing,
                    opt => opt.MapFrom(lfDto => lfDto.Proccesing))
                .ForMember(lfCommand => lfCommand.Checking,
                    opt => opt.MapFrom(lfDto => lfDto.Checking))
                .ForMember(lfCommand => lfCommand.Closed,
                    opt => opt.MapFrom(lfDto => lfDto.Closed));
        }
    }
}
