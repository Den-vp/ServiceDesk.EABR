﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.CreateService;
using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.WebApi.Models.ServiceModel
{
    public class CreateServiceDto : IMapWith<CreateServiceCommand>
    {
        [Required]
        public string Name { get; set; }
        public string Subtitle { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CreateServiceDto, CreateServiceCommand>()
                .ForMember(serCommand => serCommand.Name,
                    opt => opt.MapFrom(serDto => serDto.Name))
                .ForMember(serCommand => serCommand.Subtitle,
                    opt => opt.MapFrom(serDto => serDto.Subtitle));
        }
    }
}
