﻿using AutoMapper;
using ServiceDesk.Application.Common.Mappings;
using ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.UpdateService;
using System;

namespace ServiceDesk.WebApi.Models.ServiceModel
{
    public class UpdateServiceDto : IMapWith<UpdateServiceCommand>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateServiceDto, UpdateServiceCommand>()
                .ForMember(serCommand => serCommand.Id,
                    opt => opt.MapFrom(serDto => serDto.Id))
                .ForMember(serCommand => serCommand.Name,
                    opt => opt.MapFrom(serDto => serDto.Name))
                .ForMember(serCommand => serCommand.Subtitle,
                    opt => opt.MapFrom(serDto => serDto.Subtitle));
        }
    }
}
