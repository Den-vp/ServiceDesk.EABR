﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.CreateService;
using ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.DeleteService;
using ServiceDesk.Application.ServiceDesk.Commands.ServiceCommand.UpdateService;
using ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceDetails;
using ServiceDesk.Application.ServiceDesk.Queries.ServiceQuery.GetServiceList;
using ServiceDesk.WebApi.Models.ServiceModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]

    public class ServiceController : BaseController
    {
        private readonly IMapper _mapper;

        public ServiceController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets the list of services
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /service
        /// </remarks>
        /// <param name="nameCat">StartsWith Service name</param>
        /// <returns>Returns ServiceListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ServiceListVm>> GetAllStartsWith(string nameCat)
        {
            var query = new GetServiceListQuery
            {
                Name = nameCat
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets the service by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /service/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">Catrgory id (guid)</param>
        /// <returns>Returns ServiceDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<ServiceDetailsVm>> Get(Guid id)
        {
            var query = new GetServiceDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates the service
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /service
        /// {
        ///     CabNumber: "CabNumber",
        ///     DepartmentId: "ID Department"
        /// }
        /// </remarks>
        /// <param name="createServiceDto">createServiceDto object</param>
        /// <returns>Returns id (guid)</returns>
        /// <response code="201">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Guid>> CreateAsync([FromBody] CreateServiceDto createServiceDto)
        {
            var command = _mapper.Map<CreateServiceCommand>(createServiceDto);
            command.Name = createServiceDto.Name;
            command.Subtitle = createServiceDto.Subtitle;

            var serId = await Mediator.Send(command);
            //return Ok(serId);
            return Created(serId.ToString(), serId);
        }

        /// <summary>
        /// Updates the service
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PUT /service
        /// {
        ///     Name: "updated Name"
        /// }
        /// </remarks>
        /// <param name="updateServiceDto">updateServiceDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateServiceDto updateServiceDto)
        {
            var command = _mapper.Map<UpdateServiceCommand>(updateServiceDto);
            command.Name = updateServiceDto.Name;
            command.Subtitle = updateServiceDto.Subtitle;

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes the service by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /service/88DEB432-062F-43DE-8DCD-8B6EF79073D3
        /// </remarks>
        /// <param name="id">Id of the Service (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteServiceCommand
            {
                Id = id,
            };
            await Mediator.Send(command);
            return NoContent();
        }

    }
}
