﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.CreateDepartment;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.DeleteDepartment;
using ServiceDesk.Application.ServiceDesk.Commands.DepartmentCommand.UpdateDepartment;
using ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentDetails;
using ServiceDesk.Application.ServiceDesk.Queries.DepartmentQuery.GetDepartmentList;
using ServiceDesk.WebApi.Models.DepartmentModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class DepartmentController : BaseController
    {
        private readonly IMapper _mapper;

        public DepartmentController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets the list of departments
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /department
        /// </remarks>
        /// <param name="nameDep">StartsWith Department name</param>
        /// <returns>Returns DepartmentListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<DepartmentListVm>> GetAllStartsWith(string nameDep)
        {
            var query = new GetDepartmentListQuery
            {
                Name = nameDep
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets the department by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /department/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">Department id (guid)</param>
        /// <returns>Returns DepartmentDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<DepartmentDetailsVm>> Get(Guid id)
        {
            var query = new GetDepartmentDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates the department
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /department
        /// {
        ///     Name: "Department name"
        /// }
        /// </remarks>
        /// <param name="createDepartmentDto">CreateDepartmentDto object</param>
        /// <returns>Returns id (guid)</returns>
        /// <response code="201">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateDepartmentDto createDepartmentDto)
        {
            var command = _mapper.Map<CreateDepartmentCommand>(createDepartmentDto);
            command.Name = createDepartmentDto.Name;
            var depId = await Mediator.Send(command);
            return Ok(depId);
        }

        /// <summary>
        /// Updates the department
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PUT /department
        /// {
        ///     title: "updated Department name"
        /// }
        /// </remarks>
        /// <param name="updateDepartmentDto">UpdateDepartmentDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateDepartmentDto updateDepartmentDto)
        {
            var command = _mapper.Map<UpdateDepartmentCommand>(updateDepartmentDto);
            command.Name = updateDepartmentDto.Name;
            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes the department by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /department/88DEB432-062F-43DE-8DCD-8B6EF79073D3
        /// </remarks>
        /// <param name="id">Id of the Department (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteDepartmentCommand
            {
                Id = id,
            };
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
