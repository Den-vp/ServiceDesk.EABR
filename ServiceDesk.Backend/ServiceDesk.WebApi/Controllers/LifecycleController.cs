﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.CreateLifecycle;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.DeleteLifecycle;
using ServiceDesk.Application.ServiceDesk.Commands.LifecycleCommand.UpdateLifecycle;
using ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleDetails;
using ServiceDesk.Application.ServiceDesk.Queries.LifecycleQuery.GetLifecycleList;
using ServiceDesk.WebApi.Models.LifecycleModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;


namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]
    public class LifecycleController : BaseController
    {
        private readonly IMapper _mapper;

        public LifecycleController(IMapper mapper) => _mapper = mapper;

        /// <summary>
        /// Gets the list of categories
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /category
        /// </remarks>
        /// <param name="opened">All Opened request name</param>
        /// <returns>Returns LifecycleListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<LifecycleListVm>> GetAllOpened(DateTime opened)
        {
            var query = new GetLifecycleListQuery
            {
                Opened = opened
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets the lifecycle by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /lifecicle/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">Lifecycle id (guid)</param>
        /// <returns>Returns LifecycleDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<LifecycleDetailsVm>> Get(Guid id)
        {
            var query = new GetLifecycleDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates the lifecycle
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /lifecycle
        /// {
        ///     Opened: "Opened",
        ///     Distributed: "Distributed",
        ///     Proccesing: "Proccesing",
        ///     Checking: "Checking"
        /// }
        /// </remarks>
        /// <param name="createLifecycleDto">createLifecycleDto object</param>
        /// <returns>Returns id (guid)</returns>
        /// <response code="201">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CreateAsync([FromBody] CreateLifecycleDto createLifecycleDto)
        {
            var command = _mapper.Map<CreateLifecycleCommand>(createLifecycleDto);
            command.Opened = createLifecycleDto.Opened;
            command.Distributed = createLifecycleDto.Distributed;
            command.Proccesing = createLifecycleDto.Proccesing;
            command.Checking = createLifecycleDto.Checking;

            var lfId = await Mediator.Send(command);
            //return Ok(lfId);
            //return CreatedAtAction(nameof(Get), new { id = lfId }, createLifecycleDto);
            return Created(lfId.ToString(), lfId);
        }

        /// <summary>
        /// Updates the lifecycle
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PUT /lifecycle
        /// {
        ///     Opened: "Opened"
        ///     Distributed: "Distributed",
        ///     Proccesing: "Proccesing",
        ///     Checking: "Checking",
        ///     Closed: "Closed"
        /// }
        /// </remarks>
        /// <param name="updateLifecycleDto">updateLifecycleDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateLifecycleDto updateLifecycleDto)
        {
            var command = _mapper.Map<UpdateLifecycleCommand>(updateLifecycleDto);
            command.Opened = updateLifecycleDto.Opened;
            command.Distributed = updateLifecycleDto.Distributed;
            command.Proccesing = updateLifecycleDto.Proccesing;
            command.Checking = updateLifecycleDto.Checking;
            command.Closed = updateLifecycleDto.Closed;

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes the lifecycle by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /lifecycle/88DEB432-062F-43DE-8DCD-8B6EF79073D3
        /// </remarks>
        /// <param name="id">Id of the Lifecycle (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteLifecycleCommand
            {
                Id = id,
            };
            await Mediator.Send(command);
            return NoContent();
        }

    }
}
