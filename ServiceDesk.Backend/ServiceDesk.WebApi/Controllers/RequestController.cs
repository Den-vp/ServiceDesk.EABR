﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.CreateRequest;
using ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.DeleteRequest;
using ServiceDesk.Application.ServiceDesk.Commands.RequestCommand.UpdateRequest;
using ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestDetails;
using ServiceDesk.Application.ServiceDesk.Queries.RequestQuery.GetRequestList;
using ServiceDesk.WebApi.Models.RequestModel;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ServiceDesk.Application.Interfaces;

namespace ServiceDesk.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/{version:apiVersion}/[controller]")]

    public class RequestController : BaseController
    {
        private readonly IMapper _mapper;
        readonly ICurrentUserService _currentUserService;

        public RequestController(IMapper mapper, ICurrentUserService currentUserService) => (_mapper, _currentUserService) = (mapper, currentUserService);

        /// <summary>
        /// Gets the list of request
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /request
        /// </remarks>
        /// <param name="userCreated">All</param>
        /// <param name="executeFl">true - is not executer</param>
        /// <returns>Returns ActivListVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<RequestListVm>> GetAllRequestUserId(string userCreated, bool executeFl)
        {
            var query = new GetRequestListQuery
            {
                ExecuterFl = executeFl,
                UserCreated = userCreated
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Gets the request by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /request/D34D349E-43B8-429E-BCA4-793C932FD580
        /// </remarks>
        /// <param name="id">Request id (guid)</param>
        /// <returns>Returns RequestDetailsVm</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user in unauthorized</response>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<RequestDetailsVm>> Get(Guid id)
        {
            var query = new GetRequestDetailsQuery
            {
                Id = id
            };
            var vm = await Mediator.Send(query);
            return Ok(vm);
        }

        /// <summary>
        /// Creates the request
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /request
        /// {
        ///     Name: "Name",
        ///     Description: "Description",
        ///     Comment: "Comment",
        ///     Status: "Status",
        ///     Priority: "Priority",
        ///     ActivId: "ActivId",
        ///     File: "File",
        ///     CategoryId: "CategoryId",
        ///     UserId: "UserId",
        ///     ExecutorId: "ExecutorId",
        ///     LifecycleId: "LifecycleId"
        /// }
        /// </remarks>
        /// <param name="createRequestDto">createRequestDto object</param>
        /// <returns>Returns id (guid)</returns>
        /// <response code="201">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateRequestDto createRequestDto)
        {
            var command = _mapper.Map<CreateRequestCommand>(createRequestDto);
            command.Name = createRequestDto.Name;
            command.Description = createRequestDto.Description;
            command.Comment = createRequestDto.Comment;
            command.Status = createRequestDto.Status;
            command.Priority = createRequestDto.Priority;
            command.ActivId = createRequestDto.ActivId;
            command.File = createRequestDto.File;
            command.ServiceId = createRequestDto.ServiceId;
            command.UserId = createRequestDto.UserId;
            command.ExecutorId = createRequestDto.ExecutorId;
            command.ExecutorName = createRequestDto.ExecutorName;
            command.ExecutorLogin = createRequestDto.ExecutorLogin;
            command.ExecutorEmail = createRequestDto.ExecutorEmail;
            command.LifecycleId = createRequestDto.LifecycleId;
            command.Opened = createRequestDto.Opened;
            command.Distributed = createRequestDto.Distributed;
            command.Proccesing = createRequestDto.Proccesing;
            command.Checking = createRequestDto.Checking;
            command.Closed = createRequestDto.Closed;

            var reqId = await Mediator.Send(command);
            return Ok(reqId);
        }

        /// <summary>
        /// Updates the Request
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PUT /request
        /// {
        ///     Name: "Name",
        ///     Description: "Description",
        ///     Comment: "Comment",
        ///     Status: "Status",
        ///     Priority: "Priority",
        ///     ActivId: "ActivId",
        ///     File: "File",
        ///     CategoryId: "CategoryId",
        ///     UserId: "UserId",
        ///     ExecutorId: "ExecutorId",
        ///     LifecycleId: "LifecycleId"
        /// }
        /// </remarks>
        /// <param name="updateRequestDto">updateRequestDto object</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Update([FromBody] UpdateRequestDto updateRequestDto)
        {
            var command = _mapper.Map<UpdateRequestCommand>(updateRequestDto);
            command.Name = updateRequestDto.Name;
            command.Description = updateRequestDto.Description;
            command.Comment = updateRequestDto.Comment;
            command.Status = updateRequestDto.Status;
            command.Priority = updateRequestDto.Priority;
            command.ActivId = updateRequestDto.ActivId;
            command.File = updateRequestDto.File;
            command.ServiceId = updateRequestDto.ServiceId;
            command.UserId = updateRequestDto.UserId;
            command.ExecutorId = updateRequestDto.ExecutorId;
            command.ExecutorName = updateRequestDto.ExecutorName;
            command.ExecutorLogin = updateRequestDto.ExecutorLogin;
            command.ExecutorEmail = updateRequestDto.ExecutorEmail;
            command.Distributed = updateRequestDto.Distributed;
            command.Proccesing = updateRequestDto.Proccesing;
            command.Checking = updateRequestDto.Checking;

            await Mediator.Send(command);
            return NoContent();
        }

        /// <summary>
        /// Deletes the request by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /request/88DEB432-062F-43DE-8DCD-8B6EF79073D3
        /// </remarks>
        /// <param name="id">Id of the Request (guid)</param>
        /// <returns>Returns NoContent</returns>
        /// <response code="204">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var command = new DeleteRequestCommand
            {
                Id = id,
            };
            await Mediator.Send(command);
            return NoContent();
        }

    }
}
