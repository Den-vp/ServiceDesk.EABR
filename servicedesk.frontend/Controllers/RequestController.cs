﻿using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using servicedesk.frontend.Domain;
using servicedesk.frontend.Models;
using servicedesk.frontend.Models.RequestModel;
using servicedesk.frontend.Models.UserModel;
using servicedesk.frontend.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace servicedesk.frontend.Controllers
{
    public class RequestController : Controller
    {
        private readonly DataManager dataManager;

        public RequestController(DataManager dataManager)
        {
            this.dataManager = dataManager;
        }

        public async Task<RequestModelList> GetRequestListAsync(string id = "", string userCreated = "", bool executerFl = false)
        {
            var model = new RequestModelList();
            var prm = !string.IsNullOrEmpty(id) ? $"/{id}" : string.IsNullOrEmpty(userCreated) ? $"?executeFl={executerFl}" : $"?userCreated={userCreated}&executeFl={executerFl}";
            var apiUrl = new Uri($"{Config.EabrApi}Request{prm}");
            var apiClient = new HttpClient();
            var responseToken = await RequestTokenAsync();
            apiClient.SetBearerToken(responseToken.AccessToken);
            //var accessToken = await HttpContext.GetTokenAsync("access_token");
            //apiClient.SetBearerToken(accessToken);

            var response = await apiClient.GetAsync(apiUrl);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
            {
                return model;
            }
            JObject json = JObject.Parse(content);
            List<RequestDto> reqList = new List<RequestDto>();
            if (json["requests"] == default) { reqList.Add(JsonConvert.DeserializeObject<RequestDto>(json.ToString())); }
            else
            {
                foreach (var item in json["requests"])
                {
                    reqList.Add(JsonConvert.DeserializeObject<RequestDto>(item.ToString()));
                }
            }
            model.ListRequests = reqList;
            return model;
        }

        public async Task<RequestModelList> GetRequestListNoExecuterAsync(string userCreated)
        {
            var model = new RequestModelList();
            var prm = string.IsNullOrEmpty(userCreated) ? "" : $"?userCreated={userCreated}";
            var apiUrl = new Uri($"{Config.EabrApi}Request/notExecuter{prm}");
            var apiClient = new HttpClient();
            var responseToken = await RequestTokenAsync();
            apiClient.SetBearerToken(responseToken.AccessToken);
            var response = await apiClient.GetAsync(apiUrl);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
            {
                return model;
            }
            JObject json = JObject.Parse(content);
            List<RequestDto> reqList = new List<RequestDto>();
            if (json["requests"] == default) { reqList.Add(JsonConvert.DeserializeObject<RequestDto>(json.ToString())); }
            else
            {
                foreach (var item in json["requests"])
                {
                    reqList.Add(JsonConvert.DeserializeObject<RequestDto>(item.ToString()));
                }
            }
            model.ListRequests = reqList;
            return model;
        }

        // GET: RequestController
        public async Task<ActionResult> IndexAsync(string id = "", bool executerfl = false)
        {
            RequestModelList model = new();
            try
            {
                string userCreated = !User.IsInRole("admin") ? User.Identity.Name : "";
                model = await GetRequestListAsync(id, userCreated, executerfl);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { code = ex });
            }
            return View(model);
        }

        public async Task<ActionResult> RequestViewAsync(string id)
        {
            RequestModelList model = new();
            try
            {
                model = await GetRequestListAsync(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { code = ex });
            }
            return View(model);
        }

        // GET: RequestController/Details/5
        public async Task<UserModelList> GetUserInfoAsync(string userName)
        {
            var model = new UserModelList();
            var prm = string.IsNullOrEmpty(userName) ? "" : $"?userName={userName}";
            var apiUrl = new Uri($"{Config.EabrApi}User{prm}");
            var apiClient = new HttpClient();
            var responseToken = await RequestTokenAsync();
            apiClient.SetBearerToken(responseToken.AccessToken);
            var response = await apiClient.GetAsync(apiUrl);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
            {
                return model;
            }
            JObject json = JObject.Parse(content);
            List<UserDto> reqList = new List<UserDto>();
            if (json["users"] == default) { reqList.Add(JsonConvert.DeserializeObject<UserDto>(json.ToString())); }
            else
            {
                foreach (var item in json["users"])
                {
                    reqList.Add(JsonConvert.DeserializeObject<UserDto>(item.ToString()));
                }
            }
            model.ListUsers = reqList;
            return model;
        }

        public async Task<UserDto> GetUserIdAsync(string id)
        {
            var model = new UserDto();
            var apiUrl = new Uri($"{Config.EabrApi}User/{id}");
            var apiClient = new HttpClient();
            var responseToken = await RequestTokenAsync();
            apiClient.SetBearerToken(responseToken.AccessToken);
            var response = await apiClient.GetAsync(apiUrl);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
            {
                return model;
            }
            JObject json = JObject.Parse(content);
            model = JsonConvert.DeserializeObject<UserDto>(json.ToString());
            return model;
        }
        // GET: RequestController/EditAsync/5
        public async Task<IActionResult> EditAsync(string serviceId, string requestId)
        {
            RequestDto model = new();
            try
            {
                if (!string.IsNullOrEmpty(requestId))
                {
                    var modellist = await GetRequestListAsync(requestId, "");
                    model = modellist.ListRequests.FirstOrDefault();
                }
                var modelUserList = await GetUserInfoAsync("");
                SelectList users = new SelectList(modelUserList.ListUsers, "Id", "Name");
                ViewBag.Users = users;
                SelectList services = new SelectList(dataManager.ServiceItems.GetServiceItems().ToList(), "Id", "Title");
                ViewBag.Services = services;
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { code = ex.ToString() });
            }
            return View(model);
        }

        // POST: RequestController/EditAsync/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(string serviceId, RequestDto modelRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var apiUrlRequest = new Uri($"{Config.EabrApi}Request");
                    if (string.IsNullOrEmpty(modelRequest.LifecycleId))
                    {
                        modelRequest.Opened = DateTime.Now;
                        modelRequest.LifecycleId = Guid.Empty.ToString();
                        var modelUserList = await GetUserInfoAsync(User.Identity.Name);
                        modelRequest.userId = modelUserList.ListUsers.FirstOrDefault().Id;
                    }
                    if (!string.IsNullOrEmpty(modelRequest.ExecutorId))
                    {
                        modelRequest.Distributed = DateTime.Now;
                        var user = await GetUserIdAsync(modelRequest.ExecutorId);
                        modelRequest.ExecutorLogin = user.Login;
                        modelRequest.ExecutorName = user.Name;
                        modelRequest.ExecutorEmail = user.Email;
                    }
                    using (var apiClient = new HttpClient())
                    {
                        var responseToken = await RequestTokenAsync();
                        apiClient.SetBearerToken(responseToken.AccessToken);
                        var response = modelRequest.LifecycleId == Guid.Empty.ToString() ? await apiClient.PostAsJsonAsync(apiUrlRequest, modelRequest) : await apiClient.PutAsJsonAsync(apiUrlRequest, modelRequest);
                        response.EnsureSuccessStatusCode();
                    }
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { code = ex.ToString() });
                }
            }
            return RedirectToAction(nameof(Index));
        }
        // GET: RequestController/DeleteAsync/1
        public async Task<ActionResult> DeleteAsync(string Id)
        {
            var response = new HttpResponseMessage();
            try
            {
                using (var apiClient = new HttpClient())
                {
                    var apiUrl = new Uri($"{Config.EabrApi}Lifecycle/{Id}");
                    var responseToken = await RequestTokenAsync();
                    apiClient.SetBearerToken(responseToken.AccessToken);
                    response = await apiClient.DeleteAsync(apiUrl);
                    response.EnsureSuccessStatusCode();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { code = $"{ex.Message.ToString()}" });
            }
            return RedirectToAction(nameof(Index));
        }
        static async Task<TokenResponse> RequestTokenAsync()
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync(Config.EabrIdentity);
            if (disco.IsError) throw new Exception(disco.Error);
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "servicedesk",
                ClientSecret = "secret",

                Scope = "ServiceDeskAPI"
            });

            if (response.IsError) throw new Exception(response.Error);
            return response;
        }
    }
}
