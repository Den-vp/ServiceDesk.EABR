﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using servicedesk.frontend.Models;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System.Net.Http.Json;
using System;
using servicedesk.frontend.Domain;

namespace servicedesk.frontend.Controllers
{
    //[Authorize(Roles = "other")
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DataManager dataManager;

        public HomeController(ILogger<HomeController> logger, DataManager dataManager)
        {
            this.dataManager = dataManager;
            this._logger = logger;
        }

        public IActionResult Index()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageIndex"));
        }

        public IActionResult Contacts()
        {
            return View(dataManager.TextFields.GetTextFieldByCodeWord("PageContacts"));
        }
        public IActionResult UserInfo()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Logout()
        {
            return SignOut("Cookies", "oidc");
        }

        public async Task<IActionResult> CallApi()
        {
            var prm = "";
            var apiUrl = new Uri($"https://localhost:44328/api/1.0/User?userName={prm}");
            var apiClient = new HttpClient();
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            apiClient.SetBearerToken(accessToken);
            var response = await apiClient.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(content))
                {
                    return View("json");
                }
                JObject json = JObject.Parse(content);
                var users = json["users"];
                ViewBag.Json = users;
            }

            return View("json");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string code)
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                Code = code
            });
        }
    }
}
