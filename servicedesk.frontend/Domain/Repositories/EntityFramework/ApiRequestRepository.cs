﻿using servicedesk.frontend.Domain.Repositories.Abstract;
using servicedesk.frontend.Models.RequestModel;
using System.Linq;

namespace servicedesk.frontend.Domain.Repositories.EntityFramework
{
    public class ApiRequestRepository : IRequestRepository
    {
        public void DeleteRequestItem(string id)
        {
            throw new System.NotImplementedException();
        }

        public RequestDto GetRequestItemById(string id)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<RequestDto> GetRequestItems()
        {
            throw new System.NotImplementedException();
        }

        public void SaveRequestItem(RequestDto entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
