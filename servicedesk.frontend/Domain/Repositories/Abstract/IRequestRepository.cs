﻿using servicedesk.frontend.Models.RequestModel;
using System.Linq;

namespace servicedesk.frontend.Domain.Repositories.Abstract
{
    public interface IRequestRepository
    {
        IQueryable<RequestDto> GetRequestItems();
        RequestDto GetRequestItemById(string id);
        void SaveRequestItem(RequestDto entity);
        void DeleteRequestItem(string id);

    }
}
