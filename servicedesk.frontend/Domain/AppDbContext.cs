﻿using Microsoft.EntityFrameworkCore;
using servicedesk.frontend.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace servicedesk.frontend.Domain
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        public DbSet<TextField> TextFields { get; set; }
        public DbSet<ServiceItem> ServiceItems { get; set; }
        public object IdentityUser { get; internal set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                CodeWord = "PageIndex",
                Title = "Главная"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                CodeWord = "PageServices",
                Title = "Наши услуги"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                CodeWord = "PageContacts",
                Title = "Контакты"
            });

        }
    }
}
