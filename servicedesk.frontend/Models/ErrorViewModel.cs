using System;

namespace servicedesk.frontend.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public string Code { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
