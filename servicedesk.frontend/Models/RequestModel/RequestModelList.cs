﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace servicedesk.frontend.Models.RequestModel
{
    public class RequestModelList
    {
        public IList<RequestDto> ListRequests { get; set; }
    }
}
