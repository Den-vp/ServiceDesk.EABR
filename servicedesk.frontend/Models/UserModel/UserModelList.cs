﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace servicedesk.frontend.Models.UserModel
{
    public class UserModelList
    {
        public IList<UserDto> ListUsers { get; set; }
    }
}
